#This program will create ROC curves for each of the individuial MLAs

#import all of the MLA classes
from Classes import cnn
from Classes import pppANN
from Classes import catboost
#from Classes import xgboost

#setup the config files

import configparser
config = configparser.ConfigParser()
config.read('config.ini')

#initilize CNNs 
subband_cnn = cnn.CNN()
pers1d_cnn = cnn.CNN()
pers2d_cnn = cnn.CNN()
dm_cnn = cnn.CNN()
pulse_ann = pppANN.PulseProfileANN()

#function to create an roc curve for a cnn
def MakeRocTF(cnn,plot,name):
    cnn.SetDevice(1)
    cnn.LoadModel(plot,'Models/' + plot + '_CNN.keras')
    cnn.LoadData('test','TestData.npy')    
    cnn.LoadData('train','StageOne.npy')
    cnn.BalanceData('test')
    cnn.ShuffleData('test')
    cnn.DataPrep()
    cnn.FlattenData('test')
    cnn.FlattenData('train')    
    cnn.TestModel()
    cnn.ROC_Curve('ROC_Curves/' + plot + '_roc.png',plot,name)

#make all of the roc curves for the CNNs
#if int(config['TESTING']['pers1dcnn']):
if config['TESTING'].getboolean('pers1dcnn'):
    MakeRocTF(pers1d_cnn,'Persistence_1D','1D Persistence CNN')
#if int(config['TESTING']['pers2dcnn']):
if config['TESTING'].getboolean('pers2dcnn'):
    MakeRocTF(pers2d_cnn,'Persistence_2D','2D Persistence CNN')
#if int(config['TESTING']['subbandcnn']):
if config['TESTING'].getboolean('subbandcnn'):
    MakeRocTF(subband_cnn,'Subband','PACMANN')
#if int(config['TESTING']['dmcnn']): 
if config['TESTING'].getboolean('dmcnn'):
    MakeRocTF(dm_cnn,'DM Plots','DM CNN')

def MakeRocANN(cnn,plot):
    cnn.SetDevice(1)
    cnn.LoadModel(plot,'Models/PPPANN.keras')
    cnn.LoadData('test','TestData.npy')    
    cnn.LoadData('train','StageOne.npy')
    cnn.BalanceData('test')
    cnn.ShuffleData('test')
    cnn.PullData()
    cnn.TestModel()
    cnn.ROC_Curve('ROC_Curves/' + plot + '_roc.png',plot,'Pulse Profile ANN')

#if int(config['TESTING']['pulseann']): 
if config['TESTING'].getboolean('pulseann'):
   MakeRocANN(pulse_ann,'Sum Prof')

subband_catboost = catboost.CatBoost()
pulse_catboost = catboost.CatBoost()

def MakeRocCat(boost,plot):
    boost.LoadModel(plot,'Models/' + plot + '_catboost.json')
    boost.LoadData('test','TestData.npy')
    boost.LoadData('train','StageOne.npy')
    boost.BalanceData('test')
    boost.BalanceData('train')
    boost.FlattenData('test')
    boost.FlattenData('train')
    boost.TestModel()
    boost.ROC_Curve('ROC_Curves/cat_' + plot + '_roc.png', plot)

#if int(config['TESTING']['subbandcat']):
if config['TESTING'].getboolean('subbandcat'):
    MakeRocCat(subband_catboost,'Subband')
#if int(config['TESTING']['pulsecat']):
if config['TESTING'].getboolean('pulsecat'):
    MakeRocCat(pulse_catboost,'Sum Prof')
