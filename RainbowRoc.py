#import MLA classes
from Classes import cnn
from Classes import pppANN
from Classes import combiner
from Classes import mla

#set up config file parsing
import configparser
config = configparser.ConfigParser()
config.read('config.ini')

#right now colors are hardcoded in, and cannot be changed in the config parser

def MakeRocCNN(mla,plot,filename,name,color):
    #mla.SetDevice(int(config['GLOBAL']['gpunum']))
    mla.LoadModel(plot,filename)
    mla.LoadData('test','TestData.npy')
    mla.LoadData('train','StageOne.npy')
    mla.BalanceData('test')
    mla.ShuffleData('test')
    mla.DataPrep()
    mla.FlattenData('test')
    mla.FlattenData('train')
    mla.TestModel()
    mla.MultiROC(plot,name,color)

def MakeRocCombiner(mla,plot,filename,name,color):
    modelpaths = []
    plots = []
    if int(config['COMBINER']['subbandcnn']):
        plots.append('subbandcnn')
        modelpaths.append(config['COMBINER']['model_directory'] + 'Subband_CNN.keras')
    if int(config['COMBINER']['pers1dcnn']):
        plots.append('pers1dcnn')
        modelpaths.append(config['COMBINER']['model_directory'] + 'Persistence_1D_CNN.keras')
    if int(config['COMBINER']['pers2dcnn']):
        plots.append('pers2dcnn')
        modelpaths.append(config['COMBINER']['model_directory'] + 'Persistence_2D_CNN.keras')
    if int(config['COMBINER']['dmcnn']):
        plots.append('dmcnn')
        modelpaths.append(config['COMBINER']['model_directory'] + 'DM Plots_CNN.keras')
    if int(config['COMBINER']['pulseann']):
        plots.append('pulseann')
        modelpaths.append(config['COMBINER']['model_directory'] + 'PPPANN.keras')
    if int(config['COMBINER']['subbandcat']):
        plots.append('subbandcat')
        modelpaths.append(config['COMBINER']['model_directory'] + 'Subband_catboost.json')
    if int(config['COMBINER']['pulsecat']):
        plots.append('pulsecat')
        modelpaths.append(config['COMBINER']['model_directory'] + 'Sum Prof_catboost.json')
    if int(config['COMBINER']['dmxboost']):
        plots.append('dmxboost')
        modelpaths.append(config['COMBINER']['model_directory'] + 'dm_xgboost.json')
    print(plots)
    #mla.SetDevice(int(config['GLOBAL']['gpunum']))
    mla.LoadModel(filename)
    mla.LoadData('test','TestData.npy')
    mla.LoadData('train','StageOne.npy')
    mla.BalanceData('test')
    mla.ShuffleData('test')
    mla.dataprep()
    mla.flatten()
    mla.LoadInputModels(plots,modelpaths)
    mla.BuildInputArray('train')
    mla.BuildInputArray('test')
    mla.TestModel()
    mla.MultiROC(plot,name,color)


if int(config['TESTING']['combiner']):
    combine = combiner.COMBINER()
    MakeRocCombiner(combine,'combiner',config['TESTING']['model_directory']+'combiner.keras','Combiner','g')
if int(config['TESTING']['subbandcnn']):
    subcnn = cnn.CNN()
    MakeRocCNN(subcnn,'Subband',config['TESTING']['model_directory']+'Subband_CNN.keras','Subband CNN','tab:orange')
if int(config['TESTING']['pers1dcnn']):
    pers1d = cnn.CNN()
    MakeRocCNN(pers1d,'Persistence_1D',config['TESTING']['model_directory']+'Persistence_1D_CNN.keras','1D Persistence','yellow')
if int(config['TESTING']['pers2dcnn']):
    pers2d = cnn.CNN()
    MakeRocCNN(pers2d,'Persistence_2D',config['TESTING']['model_directory']+'Persistence_2D_CNN.keras','2D Persistence','tab:blue')
if int(config['TESTING']['dmcnn']):
    dmcnn = cnn.CNN()
    MakeRocCNN(dmcnn,'DM Plots',config['TESTING']['model_directory']+'DM Plots_CNN.keras','Dispersion Measure CNN','tab:purple')

def MakeRocANN(ann,plot,filename,name,color):
    #mla.SetDevice(int(config['GLOBAL']['gpunum']))
    ann.LoadModel(plot,'Models/PPPANN.keras')
    ann.LoadData('test','TestData.npy')    
    ann.LoadData('train','StageOne.npy')
    ann.BalanceData('test')
    ann.ShuffleData('test')
    ann.PullData()
    ann.TestModel()
    ann.MultiROC(plot,name,color)

if int(config['TESTING']['pulseann']):
    ann = pppANN.PulseProfileANN()
    MakeRocANN(ann,'Sum Prof',config['TESTING']['model_directory']+'PPPANN.keras','Pulse Profile','tab:pink')

gen = mla.MLA
gen.MultiROCSave('','ROC_Curves/rainbow.png')
