#from Classes import predictor
from Classes import combiner
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

#initilize predictor
pred = combiner.COMBINER()
#pred.SetDevice(int(config['GLOBAL']['gpunum']))
pred.LoadData('test',config['PREDICTION']['input_file'])
pred.DataPrep()
pred.Flatten()
plots = []
modelpaths = []

if config['PREDICTION'].getboolean('subbandcnn'):
    plots.append('subbandcnn')
    modelpaths.append(config['PREDICTION']['model_directory'] + 'Subband_CNN.keras')
if config['PREDICTION'].getboolean('pers1dcnn'):
    plots.append('pers1dcnn')
    modelpaths.append(config['PREDICTION']['model_directory'] + 'Persistence_1D_CNN.keras')
if config['PREDICTION'].getboolean('pers2dcnn'):
    plots.append('pers2dcnn')
    modelpaths.append(config['PREDICTION']['model_directory'] + 'Persistence_2D_CNN.keras')
if config['PREDICTION'].getboolean('dmcnn'):
    plots.append('dmcnn')
    modelpaths.append(config['PREDICTION']['model_directory'] + 'DM Plots_CNN.keras')
if config['PREDICTION'].getboolean('pulseann'):
    plots.append('pulseann')
    modelpaths.append(config['PREDICTION']['model_directory'] + 'PPPANN.keras')

pred.LoadModel(config['PREDICTION']['model_directory'] + 'combiner.keras')
pred.LoadInputModels(plots,modelpaths)
pred.BuildInputArray('test')
pred.TestModel()
#pred.Predictions()
#pred.SavePreds()
#pred.SaveArray(config['PREDICTION']['output_file'])
