#!/bin/bash

#threads=50 #the number of threads to open up 

for i in {0..50}
do
	xvfb-run -a -e /home/masters1/xfb.txt python labeling_new.py -p $i && curl -d "labeling successful" ntfy.sh/pacmann-labeling || curl -d "labeling failed" ntfy.sh/pacmann-labeling &
	sleep 10 #allows for time for things to show up in the database
done

