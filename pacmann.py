#this is a "trunk" program that mainly takes arguments and runs 
import argparse
import os

#other python files this file runs


parser = argparse.ArgumentParser(
                    prog='PACMANN',
                    description='a MLA to detect pulsars in radio data.')

parser.add_argument('-e','--edit',action='store_true',help='edit the configuration including which models are trained, and for how many epochs.')
parser.add_argument('-H','--hypertune',action='store_true',help='trains the models using hypertuning')
parser.add_argument('-t','--train',action='store_true',help='trains the models with existing structure in models')
parser.add_argument('-c','--combiner',action='store_true',help='trains the combiner with existing structure in model')
parser.add_argument('-C','--tuned_combiner',action='store_true',help='trains the combiner with hypertuning')

args = parser.parse_args()

if args.hypertune:
    import hypertrain
    
if args.trian:
    import train

if args.tuned_combiner:
    import tune_combiner.py

if args.edit:
    os.system('vim config.ini') #the way this is done needs to be changed. Ideally user's default text editor

