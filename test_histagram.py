import numpy as np
import matplotlib.pyplot as plt
import configparser
from Classes import combiner

config = configparser.ConfigParser()
config.read('config.ini')

model_directory = config['TESTING']['model_directory']
modelpaths = []
plots = []

if config['COMBINER'].getboolean('subbandcnn'):
    plots.append('subbandcnn')
    modelpaths.append(config['COMBINER']['model_directory'] + 'Subband_CNN.keras')
if config['COMBINER'].getboolean('pers1dcnn'):
    plots.append('pers1dcnn')
    modelpaths.append(config['COMBINER']['model_directory'] + 'Persistence_1D_CNN.keras')
if config['COMBINER'].getboolean('pers2dcnn'):
    plots.append('pers2dcnn')
    modelpaths.append(config['COMBINER']['model_directory'] + 'Persistence_2D_CNN.keras')
if config['COMBINER'].getboolean('dmcnn'):
    plots.append('dmcnn')
    modelpaths.append(config['COMBINER']['model_directory'] + 'DM Plots_CNN.keras')
if config['COMBINER'].getboolean('pulseann'):
    plots.append('pulseann')
    modelpaths.append(config['COMBINER']['model_directory'] + 'PPPANN.keras')

mla = combiner.COMBINER()
mla.LoadModel(model_directory+'/combiner.keras')
mla.LoadData('test','TestData.npy')
mla.LoadData('train','StageOne.npy')
mla.BalanceData('test')
mla.BalanceData('train')
mla.LoadInputModels(plots,modelpaths)
mla.dataprep()
mla.flatten()
mla.BuildInputArray('test')
mla.BuildInputArray('train')
mla.TestModel()
print(mla.predictions)
pscore = []
npscore = []

#for result in range(len(mla.predictions))
