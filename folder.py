import subprocess
import random as rand
import os
from numba import jit, cuda
import numpy as np
import sys
import statistics
import presto.filterbank
import pyymw16
from scipy import stats
from scipy.stats import norm
import presto.prepfold as prepfold
from astropy import units as u
import astropy.coordinates as c
import argparse
import sklearn
import glob
import time

class candidate:
    def __init__(self): #TODO find initilization information avalable

    def load_fits(self,fitsfile):
        from psrfits2fil import main as psr2fil
        self.fileroot=fitsfile.removesuffix('.fits')
        psr2fil(fitsfile,self.fileroot+'.fil',32,True,True,True) #make a .fil file from the fits file
        self.fil = presto.filterbank.FilterbankFile(self.fileroot+'.fil')
        self.header = self.fil.header
        return

    def make_mask(self):
        os.system('rfifind -time 2.0 -o %s %s' %(fileroot, fileroot+'.fil')) #make the file to mask RFI
        return
    
    def make_pfd(self,p,DM,filename,outifle,mask):
        nb
