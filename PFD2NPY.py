import numpy as np
from presto.prepfold import pfd
import matplotlib
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy import interpolate as interp
import configparser
from Classes import candidate
import os

config = configparser.ConfigParser()
config.read('config.ini')

directory = config['LABELING']['input_directory']
output_directory = config['LABELING']['output_directory'] 
output_file = config['LABELING']['output_file']

os.chdir(directory) #changes directory to one full of pfd files
files = os.listdir(".") # gets list of files in directory
number = 0
candidates = []
for cand in files:
    #initilize the given candidate 
    if number == 0:
        candidates.append(candidate.CANDIDATE(cand))
    else:
        candinst = candidate.CANDIDATE(cand,*candinst.return_lists())
    #candidates[number].unpack_and_interp()
    candinst.unpack_and_interp()
    print(number)
    if number%1000==0: #checkpointing
        try:
            candinst.makedata(number)
            candinst.output(output_file)
        except:
            raise('checkpoint error')
    number += 1
os.chdir(output_directory) #goes back to previous directory to save numpy file
candinst.makedata()
candinst.output(output_file)
