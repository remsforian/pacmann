# PACMANN: A PulsAr Classifier - Machine-learning Algorithm with Neural Networks

## Dependencies

- `conda` which can be installed [here](https://www.anaconda.com/download)
- This package uses [presto](https://github.com/scottransom/presto/tree/master/python) which must be installed on the system
	- Note that presto uses [pgplot](https://sites.astro.caltech.edu/~tjp/pgplot/) which is also a pacmann dependancy
- There is an optional dependancy of [mysql](https://www.mysql.com) to use the functions wherein pulsars are automatically added to a dataase

# getting started

Pacmann is a machine learning algorithm designed to identify pulsars in radio telescope data. You need conda to get started, see install instructions [here](https://www.anaconda.com/download). 

Once Conda is installed you can clone the repo and install the correct conda environment.

```
git clone https://gitlab.com/remsforian/pacmann
cd pacmann
conda env create -n pacmann -f pacmann.yml 
conda activate pacmann
```

## plots

Pacmann is comprised of several "plots" evaluated using different MLAs. The plots that we evaluate are:
1. Subband 
2. DM
3. Persistance
4. Pulse Profile (sometimes refered to as sum prof)

For human ranking of pulsar candidates we use a "megaplot" to look at all of these plots. An example of a very strong pulsar is provided here.

![a megaplot produced by prepfold of a very strong pulsar](strong_pulsar.png)

## config file

pacmann utilizes a configuration file that will control the actions of pacmann. You can edit the config file with `python pacmann -e` or by editing the `config.ini` file. 

The config file is broken down into several categories. 

#### global 
this category controls things used by the entire program, like the location of training data split into two stages. Stage one is utilized by most of the component MLAs while stage two is utilized by only the combiner. Test is utilized when evaluating the performance of the MLAs

#### tuning 

This category controls the actions used when the MLA is hypertuning. 
Each component MLA takes a one or a zero to indicate whether or not it will be tuned and trained.
There are also settings for other things such as the number of epochs, the early stopping patience, and the number of tuning runs to do.

#### training

This category controls which MLAs will be trained when not hypertuning.
Most options are the same as in the tuning section, excluding options about how the tuning functions.

#### combiner

This category controls how the combiner algorithm function. Make sure that `model_directory` is set to the directory containing the component MLAs.
The binary numbers indicate whether or not each plot will be included in the combiner algorithm. 

#### testing

This category controls how the MLA is tested. The data file needs to include labels for the test to work correctly. The binary input models **must** match your trained combiner. 

#### NTFY

NTFY is an application that can notify you when you when a given run is completed. You can read more about it [here.](https://ntfy.sh) Set a unique url that you will put on your notification device (often a mobile phone). If you wish to opt out of this feature you can just set `notify=0`

## training pacmann

If you want to train pacmann on your own data you can run `python hypertrain.py` which will do a full hyperparamiter tuning run in addition to traditional training. 

## Making Predictions

PACMANN is designed to work with `numpy` arrays, but can includes a program designed to pull the data from `pfd` files. Note that for execution over ssh X11 forwarding must be enabled. 

First use the configuration file to set the
