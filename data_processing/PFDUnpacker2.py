import numpy as np
from presto.prepfold import pfd
from ubc_AI.data import dataloader
from ubc_AI.data import pfddata
from ubc_AI.samples import normalize, downsample
import matplotlib
import matplotlib.pyplot as plt
from PIL import Image
#from resizeimage import resizeimage
import math
from scipy import interpolate
from scipy.interpolate import griddata
from scipy.interpolate import interp1d
import scipy.interpolate as interp
import random
from sklearn import preprocessing
import os

plt.switch_backend('agg')
#writes to file instead of displaying since this is an ssh code
def unpack_and_interp(pfd_data, i, DM, chi, RA, DEC, slist, plist, DMplot, SumProfFinal, p1, p2, p3):
    DM.append(pfd_data.bestdm)
    chi.append(pfd_data.calc_redchi2())
    p1.append(pfd_data.bary_p1)
    p2.append(pfd_data.bary_p2)
    p3.append(pfd_data.bary_p3)
    RA.append(pfd_data.rastr.decode('UTF-8'))
    DEC.append(pfd_data.decstr.decode('UTF-8'))
    
    return(DM, chi, RA, DEC, p1, p2, p3)

def greyscale(img):
    global_max = np.maximum.reduce(np.maximum.reduce(img))
    min_parts = np.minimum.reduce(img, 1)
    img = (img-min_parts[:,np.newaxis])/global_max
    return img

def empty_lists():
    DM = []
    chi = []
    RA = []
    DEC = []
    slist = []
    plist = []
    DMplot = []
    SumProfFinal = []
    p1 = []
    p2 = []
    p3 = []
    return(DM, chi, RA, DEC, slist, plist, DMplot, SumProfFinal, p1, p2, p3)

DM, chi, RA, DEC, slist, plist, DMplot, SumProfFinal, p1, p2, p3 = empty_lists()

file1 = open("corrupted_pfds.txt", "w")
L = ["These are the corrupted pfd files\n"]
file1.writelines(L)
file1.close()
j = 0
k = 0
l = 0
#best DM, RA, DEC, Subband, subband chi, persistence, DM, sumprof, bary_p1+2+3, stats? what is this??
for w in range(2):
    if w == 0:
        ldf = dataloader("ranked_class6-7names.txt") #loads in the non-pulsar files
        out_arraypulsar = np.ones((len(ldf.pfds), 1)) #creates an array of zeroes with dimensions pfds x 1i
#    if w == 1:
#        ldf = dataloader("/home/curren1/plsr_mla/spring_2021/data_processing/injected_class6-7.txt") #loads in the non-pulsar files
#        out_array = np.ones((len(ldf.pfds), 1)) #creates an array of zeroes with dimensions pfds x 1
    if w == 1:
        ldf = dataloader("ranked_class4-5names.txt") #loads in the pulsar files
        out_arrayRFI = np.zeros((len(ldf.pfds), 1)) #creates an array of zeroes with dimensions pfds x 1     
    for i in range(1000):
    #for i in range(4000):
        try:
            pfd_data = pfddata(ldf.pfds[i].pfdfile) #loads in each pfd file
            DM, chi, RA, DEC, p1, p2, p3 = unpack_and_interp(pfd_data, i, DM, chi, RA, DEC, slist, plist, DMplot, SumProfFinal, p1, p2, p3)
            print(i) #prints the pfd file number
        except ValueError:
            print("Corrupted PFD file")
            file2write=open("corrupted_filenames.txt",'a')
            file2write.write("%s" % pfd_data.filenm)
            file2write.close()
            if w == 0:
                out_arraypulsar = np.delete(out_arraypulsar, i, 0)
#            if w == 1:
#                l+=1
            if w == 1:
                out_arrayRFI = np.delete(out_arrayRFI, i, 0)

#out_arraypulsar = np.ones(4000-j)
#out_arrayRFI = np.zeros(4000-k)
#out_arrayinject = np.ones(4000-l)


#train_labels = np.concatenate((out_arraypulsar, out_arrayRFI, out_arrayinject))

np.savetxt("plsr_nums.txt", (DM, chi, p1, p2, p3))
