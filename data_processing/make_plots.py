import os

src_directory = '/home/masters1/pacmann/data_processing/gbncc_class4-5'
dest_directory = '/home/masters1/pacmann/data_processing/gbncc_class4-5_imgs' 

os.chdir(dest_directory) #moves us into the directory with the pfds
allfiles = os.listdir(src_directory)
pfdfiles = [files for files in allfiles if files.endswith('.pfd')] #gets all of the files with the pfd extention
for candidate in pfdfiles:
    os.system('show_pfd ' + src_directory + '/' + candidate)
