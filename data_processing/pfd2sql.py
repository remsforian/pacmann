import os
import mysql.connector as sq

directory = '/home/masters1/pacmann/data_processing/gbncc_class4-5'
database = 'pulsars'

os.chdir(directory)
allfiles = os.listdir('.')
pfdfiles = [files for files in allfiles if files.endswith('.pfd')] #goes through all files in directory and pulls out pfd files

pulsarsdb = sq.connect(
        host = 'localhost',
        user = 'masters1',
        password = 'pacmann!'
        )

cursor = pulsarsdb.cursor()
cursor.execute("use pulsars;") #puts us in the correct db
#cursor.execute("INSERT INTO pfds VALUES ('guppi_58383_GBNCC60418_0289_0001_DM11.78_Z50_ACCEL_Cand_1', '/home/masters1/pacmann/data_processing/gbncc_class4-5', LOAD_FILE('/home/masters1/pacmann/data_processing/gbnnc_class4-5/guppi_58383_GBNCC60418_0289_0001_DM11.78_Z50_ACCEL_Cand_1.pfd') );") #est execute
for candidate in pfdfiles:
    cursor.execute("INSERT INTO pfds VALUES ('" + candidate.removesuffix(".pfd") + "', '" + directory + "', '" + directory + candidate + "' );") #adds each candidate to the database
pulsarsdb.commit() #commit all changes to database
