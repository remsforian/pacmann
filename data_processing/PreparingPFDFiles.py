import numpy

filenames = numpy.loadtxt('files_pulsar.txt', dtype = str)
new = []

for filename in filenames:
	new.append("%s %d %d %d %d %d" % (filename, 1, 1, 1, 1, 1))

numpy.savetxt('ranked_class6-7names.txt', new, fmt="%s")

filenamesRFI = numpy.loadtxt('files_nonpulsar.txt', dtype = str)
newRFI = []

for filenameRFI in filenamesRFI:
        newRFI.append("%s %d %d %d %d %d" % (filenameRFI, 0, 0, 0, 0, 0))


numpy.savetxt('ranked_class4-5names.txt', newRFI, fmt="%s")
