"""Current Version"""
from astropy.io import ascii
import numpy as np
import sys
import os

#loads in the data as an astropy table
cand_n_rank = ascii.read("classifications_pdmcandid.txt")
#sorts the candidates by cand. id number
ponp=[]

#for loop that creates a list of -1 and 1 based on the ranking
for i in range(len(cand_n_rank)):
    if cand_n_rank[i][2]==4 or cand_n_rank[i][2]==5:
        ponp.append(1)
    else:
        ponp.append(0)
        
#initializing with values since the code I wrote doesn't work on the first element
short_cand_id = [255331]
np_amt = [1]
j=0

#creates two new lists. One with just the ordered unique candidate ids and one with the summation of all of the scores
#for a particular id.
q = [1]
for i in range(len(cand_n_rank)-1):
    i+=1
    if cand_n_rank[i][0]>cand_n_rank[i-1][0]:
        q.append(1)
        short_cand_id.append(cand_n_rank[i][0])
        np_amt.append(ponp[i])
        j+=1
    else:
        np_amt[j]+=ponp[i]
        q[j]+=1

#assigns each cand id a "q-score."
#a q-score is the percentage of candidates ranked as puslsars
q_score = []
for i in range(len(np_amt)):
    q_score.append((q[i]-np_amt[i])/q[i])
    
#concatenating q-score with candidate ids
q_arr = np.array(q_score)
cand_arr = np.array(short_cand_id)
q_arr = q_arr.reshape((len(q_arr), 1))
cand_arr = cand_arr.reshape((len(cand_arr), 1))
final_array = np.concatenate((cand_arr, q_arr), axis=1)

#load in candidate ids with associated filenames
files_n_id = ascii.read("pfdfiles_pdmcandid.txt")

#turns cand ids from void to ints
fni_arr = files_n_id.as_array()
ids_2 = []
for i in range(len(fni_arr)):
    ids_2.append(fni_arr[i][0])
ids2_arr = np.array(ids_2)

#finds the indices where the cand ids match
xy, x_ind, y_ind = np.intersect1d(cand_arr, ids2_arr, return_indices=True)

scores = []
files = []
scores_p = []
files_p = []
scores_d = []
files_d = []
scores_np = []
files_np = []

#assigns q-scores and files to the same indices
for i in range(len(x_ind)):
        scores.append(final_array[:,1][x_ind[i]])
        files.append(files_n_id[y_ind[i]][1])
        
#assigns the files and scores based on the scores
for i in range(len(scores)):
    if scores[i]>0.6:
        scores_p.append(scores[i])
        files_p.append(files[i])
    if 0.4<=scores[i]<=0.6:
        scores_d.append(scores[i])
        files_d.append(files[i])
    if scores[i]<0.4:
        scores_np.append(scores[i])
        files_np.append(files[i])

#lists to arrays
scores_arr_p = np.array(scores_p)
files_arr_p = np.array(files_p)
scores_arr_d = np.array(scores_d)
files_arr_d = np.array(files_d)
scores_arr_np = np.array(scores_np)
files_arr_np = np.array(files_np)

#opens the text files and converts to arrays
with open("class4-5names.txt") as f:
    c45_list = f.read().splitlines()
c45_arr = np.array(c45_list)
with open("class6-7names.txt") as f:
    c67_list = f.read().splitlines()
c67_arr = np.array(c67_list)

#checks for duplicates between files that got q-scores between 0.4 and 0.6
xy, x_ind, c67_d = np.intersect1d(files_arr_d, c67_arr, return_indices=True)
xy, x_ind, c45_d = np.intersect1d(files_arr_d, c45_arr, return_indices=True)

#removes files with q-scores between 0.4 and 0.6
p_rm_d = np.delete(c67_arr, c67_d)
np_rm_d = np.delete(c45_arr, c45_d)

#checks for misnomered files
xy, x_ind, c67_np = np.intersect1d(files_arr_np, p_rm_d, return_indices=True)
xy, x_ind, c45_p = np.intersect1d(files_arr_p, np_rm_d, return_indices=True)

#deletes misnomered files
true_p = np.delete(p_rm_d, c67_np)
true_np = np.delete(np_rm_d, c45_p)

#saves updated files
np.savetxt("files_pulsar.txt", true_p, fmt='%s')
np.savetxt("files_nonpulsar.txt", true_np, fmt='%s')
