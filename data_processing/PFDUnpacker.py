import numpy as np
from presto.prepfold import pfd
from ubc_AI.data import dataloader
from ubc_AI.data import pfddata
from ubc_AI.samples import normalize, downsample
import matplotlib
import matplotlib.pyplot as plt
from PIL import Image
#from resizeimage import resizeimage
import math
from scipy import interpolate
from scipy.interpolate import griddata
from scipy.interpolate import interp1d
import scipy.interpolate as interp
import random
from sklearn import preprocessing
import os

plt.switch_backend('agg')
#writes to file instead of displaying since this is an ssh code
def unpack_and_interp(pfd_data, i, DM, chi, RA, DEC, slist, plist, DMplot, SumProfFinal, p1, p2, p3):
    DM.append(pfd_data.bestdm)
    chi.append(pfd_data.calc_redchi2())
    p1.append(pfd_data.bary_p1)
    p2.append(pfd_data.bary_p2)
    p3.append(pfd_data.bary_p3)
    RA.append(pfd_data.rastr.decode('UTF-8'))
    DEC.append(pfd_data.decstr.decode('UTF-8'))
    
    #Begin subband 2d plot section
    z = pfd_data.profs.sum(0) #z is set to be the subband data
    z2 = greyscale(z)
    x = np.linspace(0,1,len(z.T)) #creates linspaces with lengths of the data
    y = pfd_data.subfreqs
    f = interp.interp2d(x,y,z,kind='linear')  #performs a linear interpolation of the image to make them all size 128 by 200
    xnew=np.linspace(0,1,200)
    ynew=np.linspace(300,400,128)
    zmnew=f(xnew,ynew) #creates the array
    img = greyscale(zmnew) #calls the greyscale function to reduce the array
    listimg = np.ndarray.tolist(img) #turns the img array into a list so it combines properly
    slist.append(listimg) #appends the list formatted img to slist

    #Begin persistence plot section
    z = pfd_data.profs.sum(1) #z is set to be the timephase data
    x = np.linspace(0,1,len(z.T)) #creates linspaces with lengths of the data
    y = np.linspace(0,1,len(z))
    f = interp.interp2d(x,y,z,kind='linear') #performs a linear interpolation of the image to make them all size 50 by 200
    xnew=np.linspace(0,1,50)
    ynew=np.linspace(0,1,200)
    zmnew=f(xnew,ynew) #creates the array
    img = greyscale(zmnew) #calls the greyscale function to reduce the array
    listimg = np.ndarray.tolist(img) #turns the img array into a list so it combines properly
    plist.append(listimg) #appends the list formatted img to plist

    #Begin DM plot sectio
    DMdata = pfd_data.plot_chi2_vs_DM(np.min(pfd_data.dms), np.max(pfd_data.dms), 60, pfd)
    DMlist = list(DMdata)
    DMplot.append(DMlist)

    #Begins Sumprof section
    q = pfd_data.profs.sum(1).sum(0)/np.max(pfd_data.profs.sum(1).sum(0))
    Q = (q-np.min(q))/np.max(q-np.min(q))
    x = np.linspace(0, 1, len(Q))
    f = interpolate.interp1d(x, Q)
    xnew=np.linspace(0,1,200)
    ynew = f(xnew)
    sumproflist = np.ndarray.tolist(ynew)
    SumProfFinal.append(sumproflist)
    #out_array[i][0] = ldf.target[i][0] #saves the labels to an array
    return(DM, chi, RA, DEC, slist, plist, DMplot, SumProfFinal, p1, p2, p3)

def greyscale(img):
    global_max = np.maximum.reduce(np.maximum.reduce(img))
    min_parts = np.minimum.reduce(img, 1)
    img = (img-min_parts[:,np.newaxis])/global_max
    return img

def empty_lists():
    DM = []
    chi = []
    RA = []
    DEC = []
    slist = []
    plist = []
    DMplot = []
    SumProfFinal = []
    p1 = []
    p2 = []
    p3 = []
    return(DM, chi, RA, DEC, slist, plist, DMplot, SumProfFinal, p1, p2, p3)

DM, chi, RA, DEC, slist, plist, DMplot, SumProfFinal, p1, p2, p3 = empty_lists()
DM_g, chi_g, RA_g, DEC_g, slist_g, plist_g, DMplot_g, SumProfFinal_g, p1_g, p2_g, p3_g = empty_lists()

file1 = open("corrupted_pfds.txt", "w")
L = ["These are the corrupted pfd files\n"]
file1.writelines(L)
file1.close()
j = 0
k = 0
l = 0
#best DM, RA, DEC, Subband, subband chi, persistence, DM, sumprof, bary_p1+2+3, stats? what is this??
for w in range(2):
    if w == 0:
        ldf = dataloader("ranked_class6-7names.txt") #loads in the non-pulsar files
        out_arraypulsar = np.ones((len(ldf.pfds), 1)) #creates an array of zeroes with dimensions pfds x 1i
#    if w == 1:
#        ldf = dataloader("/home/curren1/plsr_mla/spring_2021/data_processing/injected_class6-7.txt") #loads in the non-pulsar files
#        out_array = np.ones((len(ldf.pfds), 1)) #creates an array of zeroes with dimensions pfds x 1
    if w == 1:
        ldf = dataloader("ranked_class4-5names.txt") #loads in the pulsar files
        out_arrayRFI = np.zeros((len(ldf.pfds), 1)) #creates an array of zeroes with dimensions pfds x 1     
    for i, pfd in enumerate(ldf.pfds):
    #for i in range(4000):
        try:
            pfd_data = pfddata(ldf.pfds[i].pfdfile) #loads in each pfd file
            DM, chi, RA, DEC, slist, plist, DMplot, SumProfFinal, p1, p2, p3 = unpack_and_interp(pfd_data, i, DM, chi, RA, DEC, slist, plist, DMplot, SumProfFinal, p1, p2, p3)
            print(i) #prints the pfd file number
        except ValueError:
            print("Corrupted PFD file")
            file2write=open("corrupted_filenames.txt",'a')
            file2write.write("%s" % pfd_data.filenm)
            file2write.close()
            if w == 0:
                out_arraypulsar = np.delete(out_arraypulsar, i, 0)
#            if w == 1:
#                l+=1
            if w == 1:
                out_arrayRFI = np.delete(out_arrayRFI, i, 0)

#out_arraypulsar = np.ones(4000-j)
#out_arrayRFI = np.zeros(4000-k)
#out_arrayinject = np.ones(4000-l)
subarray=np.asarray(slist) #turns the list back into an array
tparray = np.asarray(plist)

sumprofarray=np.asarray(SumProfFinal)
DMarray=np.asarray(DMplot)

#train_labels = np.concatenate((out_arraypulsar, out_arrayRFI, out_arrayinject))
train_labels = np.concatenate((out_arraypulsar, out_arrayRFI))
train_labels = train_labels.reshape((len(DM),))

dm_data = DMarray.reshape((len(DM), 2, 60))

sub_data = subarray.reshape((len(DM), 128, 200))

tp_data = tparray.reshape((len(DM), 200, 50))

pp_data = sumprofarray.reshape((len(DM), 200))

print("End of Reshaping")
def split_list(a_list):
    eighth = len(a_list)//8
    return a_list[:eighth], a_list[eighth:eighth*2], a_list[eighth*2:eighth*3], a_list[eighth*3:eighth*4], a_list[eighth*4:eighth*5], a_list[eighth*5:eighth*6], a_list[eighth*6:eighth*7], a_list[eighth*7:]
train_labels1, train_labels2, train_labels3, train_labels4, train_labels5, train_labels6, train_labels7, train_labels8 = np.array_split(train_labels, 8)
DM1, DM2, DM3, DM4, DM5, DM6, DM7, DM8 = split_list(DM)
chi1, chi2, chi3, chi4, chi5, chi6, chi7, chi8 = split_list(chi)
p11, p12, p13, p14, p15, p16, p17, p18 = split_list(p1)
p21, p22, p23, p24, p25, p26, p27, p28 = split_list(p2)
p31, p32, p33, p34, p35, p36, p37, p38 = split_list(p3)
RA1, RA2, RA3, RA4, RA5, RA6, RA7, RA8 = split_list(RA)
DEC1, DEC2, DEC3, DEC4, DEC5, DEC6, DEC7, DEC8 = split_list(DEC)
sub_data1, sub_data2, sub_data3, sub_data4, sub_data5, sub_data6, sub_data7, sub_data8 = np.array_split(sub_data, 8)
tp_data1, tp_data2, tp_data3, tp_data4, tp_data5, tp_data6, tp_data7, tp_data8 = np.array_split(tp_data, 8)
pp_data1, pp_data2, pp_data3, pp_data4, pp_data5, pp_data6, pp_data7, pp_data8 = np.array_split(pp_data, 8)
dm_data1, dm_data2, dm_data3, dm_data4, dm_data5, dm_data6, dm_data7, dm_data8 = np.array_split(dm_data, 8)
print("End of split data")
pulsar_data1 = {

	"Data Length" : len(DM),
	"Train Labels" : train_labels1,
	"Pulsar Length" : len(out_arraypulsar),
	"RFI Length" : len(out_arrayRFI),
	"Best DM" : DM1,
	"Chi2" : chi1,
	"p1" : p11,
	"p2" : p21,
	"p3" : p31,
	"RA" : RA1,
	"DEC" : DEC1,
	"Subband" : sub_data1,
	"Persistence" : tp_data1,
	"Sum Prof" : pp_data1,
	"DM Plots" : dm_data1
}

pulsar_data2 = {

        "Train Labels" : train_labels2,
        "Best DM" : DM2,
        "Chi2" : chi2,
        "p1" : p12,
        "p2" : p22,
        "p3" : p32,
        "RA" : RA2,
        "DEC" : DEC2,
        "Subband" : sub_data2,
        "Persistence" : tp_data2,
        "Sum Prof" : pp_data2,
        "DM Plots" : dm_data2
}

pulsar_data3 = {

        "Train Labels" : train_labels3,
        "Best DM" : DM3,
        "Chi2" : chi3,
        "p1" : p13,
        "p2" : p23,
        "p3" : p33,
        "RA" : RA3,
        "DEC" : DEC3,
        "Subband" : sub_data3,
        "Persistence" : tp_data3,
        "Sum Prof" : pp_data3,
        "DM Plots" : dm_data3
}

pulsar_data4 = {

        "Train Labels" : train_labels4,
        "Best DM" : DM4,
        "Chi2" : chi4,
        "p1" : p14,
        "p2" : p24,
        "p3" : p34,
        "RA" : RA4,
        "DEC" : DEC4,
        "Subband" : sub_data4,
        "Persistence" : tp_data4,
        "Sum Prof" : pp_data4,
        "DM Plots" : dm_data4
}

pulsar_data5 = {

        "Train Labels" : train_labels5,
        "Best DM" : DM5,
        "Chi2" : chi5,
        "p1" : p15,
        "p2" : p25,
        "p3" : p35,
        "RA" : RA5,
        "DEC" : DEC5,
        "Subband" : sub_data5,
        "Persistence" : tp_data5,
        "Sum Prof" : pp_data5,
        "DM Plots" : dm_data5
}

pulsar_data6 = {

        "Train Labels" : train_labels6,
        "Best DM" : DM6,
        "Chi2" : chi6,
        "p1" : p16,
        "p2" : p26,
        "p3" : p36,
        "RA" : RA6,
        "DEC" : DEC6,
        "Subband" : sub_data6,
        "Persistence" : tp_data6,
        "Sum Prof" : pp_data6,
        "DM Plots" : dm_data6
}

pulsar_data7 = {

        "Train Labels" : train_labels7,
        "Best DM" : DM7,
        "Chi2" : chi7,
        "p1" : p17,
        "p2" : p27,
        "p3" : p37,
        "RA" : RA7,
        "DEC" : DEC7,
        "Subband" : sub_data7,
        "Persistence" : tp_data7,
        "Sum Prof" : pp_data7,
        "DM Plots" : dm_data7
}

pulsar_data8 = {

        "Train Labels" : train_labels8,
        "Best DM" : DM8,
        "Chi2" : chi8,
        "p1" : p18,
        "p2" : p28,
        "p3" : p38,
        "RA" : RA8,
        "DEC" : DEC8,
        "Subband" : sub_data8,
        "Persistence" : tp_data8,
        "Sum Prof" : pp_data8,
        "DM Plots" : dm_data8
}
print("Dictionaries Created")
np.save('pulsar_data1.npy',pulsar_data1)
np.save('pulsar_data2.npy',pulsar_data2)
np.save('pulsar_data3.npy',pulsar_data3)
np.save('pulsar_data4.npy',pulsar_data4)
np.save('pulsar_data5.npy',pulsar_data5)
np.save('pulsar_data6.npy',pulsar_data6)
np.save('pulsar_data7.npy',pulsar_data7)
np.save('pulsar_data8.npy',pulsar_data8)
print("Dicts saved")
