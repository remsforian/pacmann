import os
import mysql.connector as sq

directory = '/home/masters1/pacmann/data_processing/gbncc_class4-5_imgs'
database = 'pulsars'

os.chdir(directory)
allfiles = os.listdir('.')
pngfiles = [files for files in allfiles if files.endswith('.png')] #goes through all files in directory and pulls out pfd files

pulsarsdb = sq.connect(
        host = 'localhost',
        user = 'masters1',
        password = 'pacmann!'
        )

cursor = pulsarsdb.cursor()
cursor.execute("use pulsars;") #puts us in the correct db
for candidate in pngfiles:
    cursor.execute("INSERT INTO pngs VALUES ('" + candidate.removesuffix(".pfd.png") + "', '" + directory + "', '" + directory + candidate + "' );") #adds each candidate to the database
pulsarsdb.commit() #commit all changes to database
