import sys
import sys
sys.path.append('../')
import cltd as cltd
import numpy as np
from sklearn.model_selection import train_test_split



pulsar_data, nonp_data = cltd.dataloader('pulsar_data1.npy', 'pulsar_data2.npy', 'pulsar_data3.npy', 'pulsar_data4.npy', 'pulsar_data5.npy', 'pulsar_data6.npy', 'pulsar_data7.npy', 'pulsar_data8.npy')


pulsar_train = {}
pulsar_test = {}
nonp_train = {}
nonp_test = {}
print(pulsar_data.keys())
pulsar_data.pop('Data Length', None)
nonp_data.pop('Data Length', None)
pulsar_data.pop('Pulsar Length', None)
nonp_data.pop('Pulsar Length', None)
pulsar_data.pop('RFI Length', None)
nonp_data.pop('RFI Length', None)
for keys in pulsar_data.keys():
    #pulsar_data[keys] = np.asarray(pulsar_data[keys])
    #nonp_data[keys] = np.asarray(nonp_data[keys])
    pulsar_train[keys], pulsar_test[keys] = train_test_split(pulsar_data[keys], test_size=1000, random_state=42)
    nonp_train[keys], nonp_test[keys] = train_test_split(nonp_data[keys], test_size=1000, random_state=42)
print(len(pulsar_train["Subband"]))
print(len(pulsar_test["Subband"]))
print(len(nonp_train["Subband"]))
print(len(nonp_test["Subband"]))

np.save('PULSAR_TRAIN.npy',pulsar_train)
np.save('NONP_TRAIN.npy',nonp_train)
np.save('PULSAR_TEST.npy',pulsar_test)
np.save('NONP_TEST.npy',nonp_test)
