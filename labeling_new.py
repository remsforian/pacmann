import numpy as np
import subprocess
from Classes import combiner
from presto.prepfold import pfd as pyfd 
from Classes import candidate
import configparser
import os
import mysql.connector
import argparse

print("RUNNING")

config = configparser.ConfigParser()
config.read('config.ini')

model_directory = config['PREDICTION']['model_directory']
modelpaths = []
plots = []
if config['COMBINER'].getboolean('subbandcnn'):
    plots.append('subbandcnn')
    modelpaths.append(config['COMBINER']['model_directory'] + 'Subband_CNN.keras')
if config['COMBINER'].getboolean('pers1dcnn'):
    plots.append('pers1dcnn')
    modelpaths.append(config['COMBINER']['model_directory'] + 'Persistence_1D_CNN.keras')
if config['COMBINER'].getboolean('pers2dcnn'):
    plots.append('pers2dcnn')
    modelpaths.append(config['COMBINER']['model_directory'] + 'Persistence_2D_CNN.keras')
if config['COMBINER'].getboolean('dmcnn'):
    plots.append('dmcnn')
    modelpaths.append(config['COMBINER']['model_directory'] + 'DM Plots_CNN.keras')
if config['COMBINER'].getboolean('pulseann'):
    plots.append('pulseann')
    modelpaths.append(config['COMBINER']['model_directory'] + 'PPPANN.keras')

parser = argparse.ArgumentParser(
        prog = 'labeler',
        description = 'labeling unknown candidates')
parser.add_argument('-p','--paralell')
args = parser.parse_args()

root_path = '/pulsar_data'
tmpdir = '/home/masters1/tmpdir'
tmpdir = '/home/masters1/tmpdir/dir_'+str(args.paralell)
gooddir = '/home/masters1/good_pulsars'
imgdir = '/home/masters1/public_html/pngs'
cands = []
db = mysql.connector.connect(
        host="localhost",
        user="masters1",
        password="pacmann!",
        database="pulsars"
)
cursor = db.cursor()
cursor.execute('select max(source_position) from predictions')#get the starting position 
i=cursor.fetchone()[0]+1
#gets a list of all tgz in the root directory
for (root,dirs,files) in os.walk(root_path):
    for file in files:
        if file.endswith('.tgz'):
            cands.append(root+'/'+file)
with open('cands.txt', 'w') as outfile:
    outfile.write('\n'.join(str(i) for i in cands))
outfile.close()
print("number remaining ",len(cands))
model=combiner.COMBINER()
model.LoadInputModels(plots,modelpaths)
model.LoadModel(config['PREDICTION']['model_directory']+'/'+'combiner.keras')
candinst=candidate.CANDIDATE()
while i < len(cands)-1:
    print("I ", i)
    os.system('rm -r '+ tmpdir)
    os.system('mkdir '+ tmpdir)
    #makes all of the pfds in a given tar file
    #try/except will skip over bad files
    if os.system('tar xvf '+cands[i]+' -C '+tmpdir) == 0:
        pass
    else:
        print("FAILED TEST")
        cursor.execute('insert into predictions (name,source_position) values("DUMMY_'+str(i)+'", '+str(i)+')')
        db.commit()
    #gets list of candidates in single unpack
    pfds=os.listdir(tmpdir)
    for pfd in pfds:
        name = pfd.removesuffix('.pfd')
        #this is code that will deal with potential duplicate entries
        candinst.clear()
        candinst.name=tmpdir+'/'+pfd
        candinst.unpack_and_interp()
        candinst.makedata()
        model.LoadArray(candinst.pulsar_data)
        model.dataprep()
        model.flatten()
        model.BuildInputArray('test')
        model.TestModel()
        if model.predictions[0][0] > 0.5:
            os.system('cp '+tmpdir+'/'+pfd+' '+gooddir)
            os.system('show_pfd ' + tmpdir + '/' + pfd)
            os.system('mv *.png ' + imgdir) #move the image into the image directory
            os.system('rm *.ps *.bestprof') #removes remnant files
            #insert the extra information into the database
            try:
                cursor.execute('INSERT INTO pfds (name, directory, filename) VALUES ("' +name+ '", "' + gooddir +'", "' + name + '.pfd")')
                cursor.execute('INSERT INTO images (dime, directory, filename) VALUES ("' +name+ '","'+imgdir +'", "' +name +'.pfd.png")')
            except:
                pass
        pfd = pyfd(tmpdir+"/"+name+'.pfd')
        ra = pfd.rastr.decode('UTF-8')
        dec = pfd.decstr.decode('UTF-8')
        ra = ra.split(":")
        dec = dec.split(":")
        ra = list(map(float,ra))
        dec = list(map(float,dec))
        ra = (ra[0] + ra[1]/60 + ra[2]/3600)
        dec = dec[0] + dec[1]/60 + dec[2]/3600
        cursor.execute('insert into predictions (name, prediction, source_position, RA, decl) values ("'+name+'", '+str(model.predictions[0][0])+', '+str(i)+', '+ str(ra)+','+str(dec)+') on duplicate key update source_position = ' + str(i))
        db.commit()
    cursor.execute('select max(source_position) from predictions')#get the starting position 
    i = cursor.fetchone()[0]+1
