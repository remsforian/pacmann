import numpy as np
from Classes import combiner
from presto.prepfold import pfd 
from Classes import candidate
import configparser
import os
import mysql.connector

config = configparser.ConfigParser()
config.read('config.ini')

model_directory = config['PREDICTION']['model_directory']
modelpaths = []
plots = []
if config['COMBINER'].getboolean('subbandcnn'):
    plots.append('subbandcnn')
    modelpaths.append(config['COMBINER']['model_directory'] + 'Subband_CNN.keras')
if config['COMBINER'].getboolean('pers1dcnn'):
    plots.append('pers1dcnn')
    modelpaths.append(config['COMBINER']['model_directory'] + 'Persistence_1D_CNN.keras')
if config['COMBINER'].getboolean('pers2dcnn'):
    plots.append('pers2dcnn')
    modelpaths.append(config['COMBINER']['model_directory'] + 'Persistence_2D_CNN.keras')
if config['COMBINER'].getboolean('dmcnn'):
    plots.append('dmcnn')
    modelpaths.append(config['COMBINER']['model_directory'] + 'DM Plots_CNN.keras')
if config['COMBINER'].getboolean('pulseann'):
    plots.append('pulseann')
    modelpaths.append(config['COMBINER']['model_directory'] + 'PPPANN.keras')

root_path = '/pulsar_data'
tmpdir = '/home/masters1/tmpdir2'
gooddir = '/home/masters1/good_pulsars'
start = 300
cands = []

db = mysql.connector.connect(
        host="localhost",
        user="masters1",
        password="pacmann!"
)

#gets a list of all tgz in the root directory
for (root,dirs,files) in os.walk(root_path):
    for file in files:
        if file.endswith('.tgz'):
            cands.append(root+'/'+file)
with open('cands.txt', 'w') as outfile:
    outfile.write('\n'.join(str(i) for i in cands))
outfile.close()
n = start
del cands[:n] #removes the candidate tarbals that we have dealt with in a previous run
for cand in cands:
    #makes all of the pfds in a given tar file
    os.system('tar xvf '+cand+' -C '+tmpdir)
    #gets list of candidates in single unpack
    pfds=os.listdir(tmpdir)
    for pfd in pfds:
        name = pfd.removesuffix('.pfd')
        candinst=candidate.CANDIDATE(tmpdir+'/'+pfd)
        candinst.unpack_and_interp()
        candinst.makedata()
        model=combiner.COMBINER(tmpdir+'/'+name)
        model.LoadArray(candinst.pulsar_data)
        model.LoadInputModels(plots,modelpaths)
        model.LoadModel(config['PREDICTION']['model_directory']+'/'+'combiner.keras')
        model.dataprep()
        model.flatten()
        model.BuildInputArray('test')
        print("TESTSHAPE",model.testpreds.shape)
        model.TestModel()
        if model.predictions[0][0] > 0.5:
            os.system('cp '+tmpdir+'/'+pfd+' '+gooddir)
        print(model.predictions[0][0])
    n += 1
    os.system('rm '+tmpdir+'/*')
