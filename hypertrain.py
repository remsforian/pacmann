from Classes import cnn
from Classes import pppANN 
from Classes import catboost
from Classes import xgboost
import os 
import configparser
import requests

#load in config file to decide which plots are trained
config = configparser.ConfigParser()
config.read('config.ini')

#get the data we are training / testing on
global traindata
global testdata
global gpu_num
gpu_num = int(config['GLOBAL']['gpunum'])
traindata = config['GLOBAL']['stageone']
testdata = config['GLOBAL']['test']

def notify(plot): #function to send out progress notifications
    #if not int(config['NTFY']['notify']): #Don't notify if disabled
    if not config['NTFY'].getboolean('notify'):
        return
    message = "Hypertuning / trining run completed on " + str(plot)
    requests.post((config['NTFY']['url']), data=message.encode(encoding='utf-8')) 
#initilize the CNNs
subband_cnn = cnn.CNN()
pers1d_cnn = cnn.CNN()
pers2d_cnn = cnn.CNN()
dm_cnn = cnn.CNN()
def hypertrain_cnn(cnn,plot): #function to hypertune and train each CNN
    cnn.SetDevice(gpu_num) #only train on a single GPU
    cnn.LoadData('train',traindata) #load in the data
    cnn.LoadData('test',testdata)
    cnn.BalanceData('train') 
    cnn.ShuffleData('train')
    cnn.DataPrep()
    cnn.FlattenData('train')
    cnn.FlattenData('test')
    cnn.Hypertune(plot,config['TUNING']['runs'],config['TUNING']['epochs'],config['TUNING']['trials'],config['TUNING']['patence']) #hypertunes the paticular cnn (number of tuning runs trials,epochs per tuning trial,trial runs per tune,patence)
    cnn.TrainModel(config['TRAINING']['epochs'],config['TRAINING']['patence'])
    cnn.SaveModel(str(config['TUNING']['model_directory']) + plot + '_CNN.keras')
    os.system('rm -r untitled_project') #deletes the checkpoint directory
    notify(plot + " cnn")   

if config['TUNING'].getboolean('subbandcnn'):
    hypertrain_cnn(subband_cnn,'Subband')
if config['TUNING'].getboolean('pers1dcnn'):
    hypertrain_cnn(pers1d_cnn,'Persistence_1D')
if config['TUNING'].getboolean('pers2dcnn'):
    hypertrain_cnn(pers2d_cnn,'Persistence_2D')
if config['TUNING'].getboolean('dmcnn'):
    hypertrain_cnn(dm_cnn,'DM Plots')

#initilize the ANN
pulse_profile_ann = pppANN.PulseProfileANN()

def hypertrain_ann(ann):
    os.system('rm -r untitled_project') #deletes the checkpoint directory
    ann.SetDevice(gpu_num) #to run on a single GPU
    ann.LoadData('train',traindata) #load in data
    ann.LoadData('test',testdata)
    ann.BalanceData('test')
    ann.BalanceData('train')
    ann.ShuffleData('train')
    ann.ShuffleData('test') 
    ann.PullData()
    ann.Hypertune(int(config['TUNING']['runs']),int(config['TUNING']['epochs']),int(config['TUNING']['trials']),int(config['TUNING']['patence'])) #hypertunes the paticular cnn (number of tuning runs trials,epochs per tuning trial,trial runs per tune,patence)
    ann.TrainModel(int(config['TRAINING']['epochs']),int(config['TRAINING']['patence']))
    ann.SaveModel(str(config['TUNING']['model_directory']) + 'pulse_ann.keras')
    notify("Pulse ann")

if config['TUNING'].getboolean('pulseann'):
    hypertrain_ann(pulse_profile_ann)

#initilize the catboosts

subband_catboost = catboost.CatBoost() 
pulse_catboost = catboost.CatBoost() 

def train_catboost(boost,plot):
    boost.SetDevice(gpu_num) #to only train on one of the GPUs
    boost.LoadData('train',traindata) #load in the data
    boost.LoadData('test',testdata)
    boost.FlattenData('train')
    boost.FlattenData('test')
    boost.BalanceData('train') #balance both kinds of data
    boost.BalanceData('test')
    boost.ShuffleData('train')
    boost.ShuffleData('test')
    boost.ValidationSplit()
    boost.BuildModel(plot)
    boost.TrainModel()
    boost.SaveModel(plot,'Models/' + plot + '_catboost.json')
    notify(plot + " cat")

#if config['TUNING']['subbandcat']:
#    train_catboost(subband_catboost,'Subband')
#if config['TUNING']['pulsecat']: 
#    train_catboost(pulse_catboost, 'Sum Prof')

#xgboosts (only for DM)

dm_xgboost = xgboost.XGBoost()

def train_xgboost(boost):
    boost.SetDevice(gpu_num) #limit to only one GPU
    boost.LoadData('train',traindata) #load in the data
    boost.LoadData('test',testdata)
    boost.BalanceData('train')#do all of the data prep stuff
    boost.BalanceData('test')
    boost.ShuffleData('train')
    boost.ShuffleData('test')
    boost.FlattenData('train')
    boost.FlattenData('test')
    boost.BuildModel()
    boost.TrainModel()
    boost.SaveModel('Models/dm_xgboost.json')
    notify(plot + " xg")

#if config['TUNING']['dmxboost']: 
#    train_xgboost(dm_xgboost)
