import subprocess
import random as rand
import os
from numba import jit, cuda
import numpy as np
import os
import sys
import statistics
import presto.filterbank
import pyymw16
from scipy import stats
from scipy.stats import norm
import presto.prepfold as prepfold
from astropy import units as u
import astropy.coordinates as c
import argparse
import sklearn
import glob
import time

class injector:
    def __init__(self,min_snr,max_snr,directory,srcdir):
        #snr range we will look at
        self.min_snr = min_snr
        self.srcdir = srcdir
        self.max_snr = max_snr
        self.directory = directory
        #arrays to hold real pulsar data
        self.ratio = []
        self.DM = []
        self.periods = []
        self.RA = []
        self.DEC = []
        self.MaxDM = []

    def load_pulsars(self,pulsars):
        #loads the numpy arrays that hold real data from which we wlil take the distributions
        self.pulsars = np.load(pulsars, allow_pickle=True)[()] #loads in the numpy pulsar dictionary
        #cast useful data from dict to our lists
        self.RA = self.pulsars["RA"]
        self.DEC = self.pulsars["DEC"]
        self.periods = self.pulsars["p1"]
        self.DM = self.pulsars["Best DM"]
        self.MaxDist = 25000 #probably best to tune this

    def pulsar_processing(self):
        #finds the maximum DM that would give a reasonable distance 
        self.dm_max = []
        self.dm = []
        self.ra = []
        self.dec = []
        self.sky_coords = []
        for ra_nounit, dec_nounit, i in zip(self.RA, self.DEC,range(len(self.RA))):
            self.dm_max.append(0)
            #creats versions of our values that have units
            self.dm.append( pyymw16.Quantity(self.dm_max[i], unit='pc cm^-3'))
            self.ra.append( c.Angle(ra_nounit, unit='hourangle'))
            self.dec.append( c.Angle(dec_nounit, unit='degree'))
            self.sky_coords.append( c.SkyCoord(self.ra[i],self.dec[i], frame='icrs'))
            #turn the DM value into a distance based on sky location
            dist, tau_sc = pyymw16.dm_to_dist(self.sky_coords[i].galactic.l, self.sky_coords[i].galactic.b, self.dm[i])
            while dist.value < self.MaxDist: 
                #creats versions of our values that have units
                self.dm[i] = pyymw16.Quantity(self.dm_max[i], unit='pc cm^-3')
                self.ra[i] = c.Angle(ra_nounit, unit='hourangle')
                self.dec[i] = c.Angle(dec_nounit, unit='degree')
                self.sky_coords[i] = c.SkyCoord(self.ra[i],self.dec[i], frame='icrs')
                #turn the DM value into a distance based on sky location
                dist, tau_sc = pyymw16.dm_to_dist(self.sky_coords[i].galactic.l, self.sky_coords[i].galactic.b, self.dm[i])
                self.dm_max[i] += 1
            #now that we have a value for max dm we can calculate some other things 
            self.ratio.append( self.DM[i]/self.dm_max[i])
            if i%10==0:
                percent=100*(i/len(self.RA))
                print("load pulsars at " + str(round(percent,2)) + "%")

        #calculating values that rely on completed lists
        self.mu_dm = sum(self.ratio)/len(self.ratio)
        self.sigma_dm = statistics.pstdev(self.ratio)
        self.mu_period = sum(self.periods)/len(self.periods)
        self.sigma_period = statistics.pstdev(self.periods)
        self.max_ratio = max(self.ratio)
        self.min_ratio = min(self.ratio)
        self.max_period = max(self.periods)
        self.min_period = min(self.periods)
        #gets a dew statistical distribution of dm and periods
        self.new_dm = stats.gaussian_kde(self.ratio).resample(10000)
        self.new_periods = stats.gaussian_kde(np.concatenate([-1*self.periods,self.periods])).resample(10000)

    def load_noise(self,fitsfile):
        from psrfits2fil import main as psr2fil
        psr2fil(fitsfile, fitsfile.removesuffix('.fits') + '.fil', 32, True, True, True) #makes a fil file from the fits file
        #saves the information in the fil file
        self.fil = presto.filterbank.FilterbankFile(fitsfile.removesuffix('.fits') + '.fil')
        self.header = self.fil.header

    def make_mask(self,filfile):
        fileroot=filfile.removesuffix('.fil')
        os.system('rfifind -time 2.0 -o %s %s' %(fileroot, filfile))

    def make_pfd_file(self,p,DM,filename,outfile,mask):
        #function to fold our data
        if (p < 0.002):
            Mp, Mdm, N = 2, 2, 24
            otheropts = "-npart 100 -pstep 10 -ndmfact 3"
        elif p < 0.05:
            Mp, Mdm, N = 2, 1, 50
            otheropts = "-npart 80 -pstep 5 -pdstep 2 -dmstep 3"
        elif p < 0.5:
            Mp, Mdm, N = 1, 1, 100
            otheropts = "-npart 30 -nodmsearch -pstep 1 -pdstep 2 -dmstep 1"
        else:
            Mp, Mdm, N = 1, 1, 200
            otheropts = "-npart 30 -nodmsearch -nopdsearch -pstep 1 -pdstep 2 -dmstep 1"
        inject_spec = " -filterbank -zerodm -ignorechan 307:410 "
        os.system("prepfold -nsub 128 -p %f -dm %f %s %s -n %d -nosearch -topo -noxwin -npfact %d -ndmfact %d -mask %s -o %s %s" %(p, DM, otheropts, inject_spec, N, Mp, Mdm, mask, outfile, filename))

    def injection(self, filename, pfddir='.'):
        fileroot = filename.removesuffix('.fil')
        too_low = False 
        for n in range(1): #putting in many pulsars for each noise event 
            #os.chdir(pfddir#moves to the directory where we want all of the pfdfiles to be
            check = 0
            sigma_inject = 0
            sigma_noise = 1
            chi_inject = 0
            chi_noise = 1
            too_high = False
            too_low = False 
            best_ratio = 0
            #set best max to a random dm in the acceptable range
            while best_ratio<self.min_ratio or best_ratio>self.max_ratio:
                best_ratio = rand.choice(self.new_dm[0])
            period = 0
            while period<self.min_period or period>self.max_period:
                period = rand.choice(self.new_periods[0])
            DM = rand.choice(self.dm_max) * best_ratio
            pfdfile = fileroot + '_cand_' + str(n)
            print("PFDFILE: ", pfdfile)
            self.make_pfd_file(period,DM,self.srcdir+'/'+filename,self.directory+'/'+pfdfile, self.srcdir+'/'+fileroot+'_rfifind.mask')
            period_seconds = period * 1000 #unit conversion
            #get the prepfold data
            pfdloc = glob.glob(self.directory+'/'+pfdfile+'*'+'.pfd')
            #try except needed because injection will throw an error about pfdloc[0] being out of range otherwize
            try:
                pfd_data=prepfold.pfd(pfdloc[0])
                sigma_noise = prepfold.pfd.calc_sigma(pfd_data)
                chi_noise = prepfold.pfd.calc_redchi2(pfd_data)
            except:
                break
            i = 0
            while sigma_inject<sigma_noise*3:
            #while chi_inject<chi_noise*2:
                mu, sigma = 3., 1.
                SNR_list = np.random.lognormal(mu, sigma, 10000)
                SNR = 0
                while SNR<self.min_snr or SNR>self.max_snr:
                    SNR = rand.choice(SNR_list)
                #inject the noise file with a pulsar 
                print("SNR:",SNR)
                os.system('./injectpsr.py --dm %f -p %f -s snr -c "snr=%f,rms=32" -v "1 200 0.5" -o %s_injected.fil %s' %(DM, period, SNR, self.srcdir+'/'+fileroot, self.srcdir+'/'+filename))
                #make the pfd file
                pfdfile = fileroot + '_injected_cand_' + str(n)
                self.make_pfd_file(period,DM,self.srcdir+'/'+fileroot+'_injected.fil',self.directory+'/'+pfdfile,self.srcdir+'/'+fileroot+'_rfifind.mask')
                print("PFDFILE: ", pfdfile)
                pfdloc=glob.glob(self.directory+'/'+pfdfile+'*'+'.pfd')
                print("PFDLOC: ", pfdloc)
                time.sleep(5) #needed to correctly load pfd_data
                try:
                    pfdloc=glob.glob(self.directory+'/'+pfdfile+'*'+'.pfd')
                    pfd_data=prepfold.pfd(pfdloc[0])
                except:
                    break
                sigma_inject=prepfold.pfd.calc_sigma(pfd_data)
                chi_inject = prepfold.pfd.calc_redchi2(pfd_data)
                print("Chi Ratio", chi_inject/chi_noise)
                print("Sigma Ratio", sigma_inject/sigma_noise)
                i += 1
                if i>10: #break out of noise file if acceptable injection cannot be made in ten times
                    deadfiles=glob.glob(self.directory+'/'+fileroot+'*'+'cand_'+str(n)+'*')
                    for n in deadfiles:
                        os.remove(n)
                    return 
            
parser = argparse.ArgumentParser(
        prog='PACMANN injector',
        description='program to inject a fake pulsar into existing noise')

parser.add_argument('-s', '--source_directory', default='/home/masters1/pacmann/fits_files')
parser.add_argument('-d', '--dest_directory', default='/home/masters1/pacmann/pfds')

args = parser.parse_args()

fitsfiles = os.listdir(args.source_directory) #get a list of all of the .fits files
injectors = []
i=0 #itteration over the list
rand.seed(42) #sets a random seed for repeatable results
for f in fitsfiles:
    if f.endswith('.fits'): #disregards any non fits files that ended up in the directory somehow
        injectors.append(injector(50,10000,args.dest_directory,args.source_directory)) #makes a list
        injectors[i].load_pulsars('/home/masters1/pulsars_data_smaller.npy')
        injectors[i].pulsar_processing()
        injectors[i].load_noise(args.source_directory+'/'+f)
        injectors[i].make_mask(args.source_directory+'/'+f.removesuffix('.fits')+'.fil')
        injectors[i].injection(f.removesuffix('.fits')+'.fil')
        i+=1
