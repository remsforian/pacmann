from Classes import combiner
import configparser
import requests

#sets up configparser
config = configparser.ConfigParser()
config.read('config.ini')
#bring in the datasets
traindata = 'StageTwo.npy'
testdata = 'TestData.npy'
gpunum = int(config['GLOBAL']['gpunum'])
plots=[]
modelpaths=[]
def notify():
    if not int(config['NTFY']['notify']):
        return
    message = "combiner done"
    requests.post((config['NTFY']['url']),data=message.encode(encoding ='utf-8'))
#add plots as decided by the config parser
if int(config['COMBINER']['subbandcnn']):
    plots.append('subbandcnn')
    modelpaths.append(config['COMBINER']['model_directory'] + 'Subband_CNN.keras')
if int(config['COMBINER']['pers1dcnn']):
    plots.append('pers1dcnn')
    modelpaths.append(config['COMBINER']['model_directory'] + 'Persistence_1D_CNN.keras')
if int(config['COMBINER']['pers2dcnn']):
    plots.append('pers2dcnn')
    modelpaths.append(config['COMBINER']['model_directory'] + 'Persistence_2D_CNN.keras')
if int(config['COMBINER']['dmcnn']):
    plots.append('dmcnn')
    modelpaths.append(config['COMBINER']['model_directory'] + 'DM Plots_CNN.keras')
if int(config['COMBINER']['pulseann']):
    plots.append('pulseann')
    modelpaths.append(config['COMBINER']['model_directory'] + 'PPPANN.keras')
if int(config['COMBINER']['subbandcat']):
    plots.append('subbandcat')
    modelpaths.append(config['COMBINER']['model_directory'] + 'Subband_catboost.json')
if int(config['COMBINER']['pulsecat']):
    plots.append('pulsecat')
    modelpaths.append(config['COMBINER']['model_directory'] + 'Sum Prof_catboost.json')
if int(config['COMBINER']['dmxboost']):
    plots.append('dmxboost')
    modelpaths.append(config['COMBINER']['model_directory'] + 'dm_xgboost.json')
print(plots)
#initilize the mla
mla = combiner.COMBINER()
#isolate gpu
mla.SetDevice(gpunum)
#load in the data
mla.LoadData('train',traindata)
mla.LoadData('test',testdata)
#format the data correctly
mla.BalanceData('train')
mla.ShuffleData('train')
mla.dataprep()
mla.flatten()
#load the input models (this stage can probably be optimized with a naming convention change)
mla.LoadInputModels(plots,modelpaths)
#make predictions based on the individuial models
mla.BuildInputArray('train')
#hypertraining run
mla.BuildCombiner(500,1000,3,20)
mla.TrainModel(1000,20)
#save the model
mla.SaveModel(config['COMBINER']['model_directory']+'combiner.keras')
notify()
