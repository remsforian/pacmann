from Classes import cnn
from Classes import pppANN
from Classes import catboost
from Classes import xgboost
import os
import configparser
import requests

config = configparser.ConfigParser()
config.read('config.ini')

def notify(plot):
    if not int(config['NTFY']['notify']): #don't send notification when off
        return
    message = 'training run completed on: ' + str(plot) 
    requests.post((config['NTFY']['url']), data=message.encode(encoding='utf-8'))

def train_cnn(cnn,plot):
    #cnn.SetDevice(config['GLOBAL']['gpunum'])
    cnn.LoadData('train',config['GLOBAL']['stageone'])
    cnn.LoadData('test',config['GLOBAL']['test'])
    cnn.BalanceData('train')
    cnn.ShuffleData('train')
    cnn.DataPrep()
    cnn.FlattenData('train')
    cnn.FlattenData('test')
    cnn.LoadModel(plot,plot+'_CNN.keras')
    cnn.TrainModel(config['TRAINING']['epochs'],config['TRAINING']['patence'])
    cnn.SaveModel(plot+'_CNN.keras')

if int(config['TRAINING']['subbandcnn']):
    model = cnn.CNN()
    train_cnn(model,'Subband')
if int(config['TRAINING']['pers1dcnn']):
    model = cnn.CNN()
    train_cnn(model,'Persistence_1D')
if int(config['TRAINING']['pers2dcnn']):
    model = cnn.CNN()
    train_cnn(model,'Persistence_2D')
if int(config['TRAINING']['dmcnn']):
    model = cnn.CNN()
    train_cnn(model,'DM Plots')

def train_ann(ann):
    ann.SetDevice(config['GLOBAL']['gpunum'])
    ann.LoadData('train',config['GLOBAL']['stageone'])
    ann.LoadData('test',config['GLOBAL']['test'])
    ann.BalanceData('train')
    ann.ShuffleData('train')
    ann.PullData()
    ann.LoadModel(config['TRAINING']['model_directory'] + 'pulse_ann.keras')
    ann.TrainModel(config['TRAINING']['epochs'],config['TRAINING']['patence'])
    ann.SaveModel(config['TRAINING']['model_directory'] + 'pulse_ann.keras')

if int(config['TRAINING']['pulseann']):
    model = pppANN.PulseProfileANN()
    train_ann(model)

def train_xgboost(xgb):
    xgb.LoadData('train', config['GLOBAL']['stageone'])
    xgb.LoadData('test', config['GLOBAL']['test'])
    xgb.BalanceData('train')
    xgb.BalanceData('test')
    xgb.FlattenData('train')
    xgb.FlattenData('test')
    xgb.BuildModel()
    xgb.TrainModel()
    xgb.SaveModel('Models/xgboost.json')
    xgb.TestModel()

if int(config['TRAINING']['dmxboost']):
    model = xgboost.XGBoost()
    train_xgboost(model)

def train_catboost(cat,plot):
    cat.LoadData('train',config['GLOBAL']['stageone'])
    cat.LoadData('test',config['GLOBAL']['test'])
    cat.BalanceData('train')
    cat.BalanceData('test')
    cat.FlattenData('train',plot)
    cat.FlattenData('test',plot)
    cat.ValidationSplit()
    cat.BuildModel(plot)
    cat.TrainModel()
    cat.SaveModel(plot,'Models/'+plot+'.json')
    cat.TestModel()
    cat.ROC_Curve('ROC_Curves/'+plot+'_cat.png',plot,plot)

if int(config['TRAINING']['subbandcat']):
    subcat = catboost.CatBoost()
    train_catboost(subcat,'Subband')

if int(config['TRAINING']['pulsecat']):
    pulsecat = catboost.CatBoost()
    train_catboost(pulsecat,'Sum Prof')
