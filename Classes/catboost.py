from . import mla
import numpy as np
from catboost import CatBoostClassifier, Pool, cv
from catboost import metrics
from sklearn.model_selection import train_test_split

class CatBoost(mla.MLA):
    def ModelFeatures(self):
        pass
    def BalanceData(self, datatype):
        if datatype == 'train':
            totaldatadict = self.traindatadict 
        elif datatype == 'test':
            totaldatadict = self.testdatadict
        
        self.DataBalanceCheck(datatype)
        poscount = self.pos
        negcount = self.neg
        print("poscount is ", poscount)
        print("negcount is ", negcount)
        print(negcount+poscount)
        baldatadict = {}
        labelsarray = []
        subplotarray = []
        pplotarray =[]
        if negcount > poscount: 
            for i in range(int(poscount+poscount)): #indexing over entire length becuase I don't know how the data is shuffled
                labelsarray.append(totaldatadict['Train Labels'][i])
                subplotarray.append(totaldatadict['Subband'][i])
                pplotarray.append(totaldatadict['Sum Prof'][i])
            print("Labelsarray:",len(labelsarray))
            baldatadict['Train Labels'] = labelsarray
            print(len(baldatadict['Train Labels']))
            self.ncount = len(baldatadict['Train Labels'])
            print("Subplot array:",len(subplotarray))
            baldatadict['Subband'] = subplotarray  
            print(len(baldatadict['Subband']))
            print("Pulseplot array:",len(pplotarray))
            baldatadict['Sum Prof'] = pplotarray  
            print(len(baldatadict['Sum Prof']))
        elif negcount == poscount:
            baldatadict['Train Labels'] = totaldatadict['Train Labels']
            baldatadict['Subband'] = totaldatadict['Subband']
            baldatadict['Sum Prof'] =totaldatadict['Sum Prof']
        if datatype == 'train':
            self.traindatadict = baldatadict
            self.trainbalance = True
        elif datatype == 'test':
            print("test")
            self.testdatadict = baldatadict
            self.testbalance = True
            
    def SaveData(self,datatype):
        if datatype == 'train':
            np.save("CatTrainSet.npy", self.traindatadict)
        elif datatype == 'test':
            np.save("CatTestSet.npy", self.testdatadict)
        #just save as dict, in init add user specified file name
            
    def FlattenData(self, datatype, plot):
        if plot=='Subband':
            if datatype == 'train':
                subdatadict = self.traindatadict 
            elif datatype == 'test':
                subdatadict = self.testdatadict
            
            subdata_array = np.array(subdatadict['Subband']) #converting the DM data into an array (no need for labels)
            datalength = len(subdata_array) #creates a variable with the number of candidates in the dataset
            subdata_flat = subdata_array.reshape(datalength,-1) # flattens the features of the data set into one axis 
            
            
            if datatype == 'train':
                self.trainsubdata = subdata_flat
                print("YES")
                self.trainlabels = subdatadict['Train Labels']
                print("train: ", np.shape(self.trainsubdata))
                
            elif datatype == 'test':
                self.testsubdata = subdata_flat
                self.testlabels = subdatadict['Train Labels']
                print("test: ", np.shape(self.testsubdata))

    def ValidationSplit(self, val_split = 0.2, randomstate = 42):
        #Splitting Subband data
        subdata = self.trainsubdata
        sublabels = self.trainlabels
        trainsubdata, valsubdata, trainsublabels, valsublabels = train_test_split(subdata, sublabels, test_size= val_split, random_state=randomstate)
        self.trainsubdata = trainsubdata
        self.trainsublabels = trainsublabels
        self.valsubdata = valsubdata
        self.valsublabels = valsublabels
        
        #Splitting Pulse Profile data
        traindata = self.traindatadict
        pulsedata = traindata['Sum Prof']
        pulselabels = traindata['Train Labels']
        trainpulsedata, valpulsedata, trainpulselabels, valpulselabels = train_test_split(pulsedata, pulselabels, test_size= val_split, random_state=randomstate)
        self.trainpulsedata = trainpulsedata
        self.trainpulselabels = trainpulselabels
        self.valpulsedata = valpulsedata
        self.valpulselabels = valpulselabels
        #Just assigning pulse test attribute, since this is done for subband only in the flatten method
        
    def BuildModel(self, plot, randomseed = 42):
        model = CatBoostClassifier(
                custom_loss=[metrics.Accuracy()],
                random_seed=randomseed,
                logging_level='Silent',
                task_type="GPU",
                devices="0")
        
        if plot == 'Subband':
            self.plot = 'Subband'
            self.submodel = model
        if plot == 'Sum Prof':
            self.plot = 'Sum Prof'
            self.pulsemodel = model
            
    def TrainModel(self,plotting = True): #plotting is whether or not catboost prints the dynamic graph
        print("sdata", np.shape(self.trainsubdata))
        if self.plot == 'Subband':
            traindata = self.trainsubdata #don't need to pull out data dict bc subdata attribute assigned when flattened
            valdata = self.valsubdata
            trainlabels = self.trainsublabels
            vallabels = self.valsublabels
            model = self.submodel
        if self.plot == 'Sum Prof':
            traindata = self.trainpulsedata
            valdata = self.valpulsedata
            trainlabels = self.trainpulselabels
            vallabels = self.valpulselabels
            model = self.pulsemodel

        traindata=np.array(traindata)
        categorical_features_indices = np.where(traindata.dtype != float)[0]    
        model.fit(traindata, trainlabels,
                cat_features=categorical_features_indices,
                eval_set=(valdata, vallabels),
                logging_level='Verbose',  # you can uncomment this for text output
                plot=plotting);
        if self.plot == 'Subband':
            self.submodel = model
        if self.plot == 'Sum Prof':
            self.pulsemodel = model
            
    def LoadModel(self, plot, filename):
        model = CatBoostClassifier()
        model.load_model(filename)
        
        if plot == 'Subband':
            self.submodel = model
            self.plot = 'Subband'
        if plot == 'Sum Prof':
            self.pulsemodel = model
            self.plot = 'Sum Prof'
        
    def TestModel(self):
        if self.plot == 'Subband':
            model = self.submodel
            testdata = self.testsubdata
        if self.plot == 'Sum Prof':
            model = self.pulsemodel
            testdata = self.testpulsedata
        
        predictions = model.predict(testdata)
        predictions_probs = model.predict_proba(testdata)
        posprobs = []
        for i in range(len(predictions_probs)):
            posprobs.append(predictions_probs[i][1])
        #I'm not making these attributes plot specific because the ROC_Curve function just uses self.predictions
        self.predictions_binary = predictions
        self.predictions = posprobs
    
    def SaveModel(self, plot, modelname):
        if plot == 'Subband':
            model = self.submodel
            model.save_model(modelname)
        if plot == 'Sum Prof':
            model = self.pulsemodel
            model.save_model(modelname)
