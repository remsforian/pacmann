from . import mla
import numpy as np
import tensorflow as tf
import keras_tuner as kt
from keras_tuner import BayesianOptimization

class PulseProfileANN(mla.MLA):
    def ModelFeatures(self, epoch = 200,validation_split=0.2, run_name = "pp_ANN"):
        self.epoch = epoch
        self.valsplit = validation_split
        self.name = run_name
  
    def PullData(self):
        #load in training data
        traindatadict = self.traindatadict
        self.trainPPdata = traindatadict['Sum Prof']
        self.trainlabels = traindatadict['Train Labels']
        #create the seporate label and pulse profile variables 
        self.trainPPdata = np.array(self.trainPPdata)
        self.trainlabels = np.array(self.trainlabels)
        
        #load in testing data 
        testdatadict = self.testdatadict
        self.testPPdata = testdatadict['Sum Prof']
        self.testlabels = testdatadict['Train Labels']
        #create the seporate label and pulse profile variables 
        self.testPPdata = np.array(self.testPPdata)
        self.testPPlabels = np.array(self.testlabels)
            
    def Hypertune(self,trials=150,ep_per_trial=10,ex_per_trial=5,pat = 5):
        #we only use an ANN for the pulse profile, so we don't need to split based on the plot
        trainPPdata = self.trainPPdata
        pulse_length = len(trainPPdata[0]) #length of a single data point
        
        #creates an array of possible learning rates to be tuned over
        learning_rate_choices = []
        for i in range (-5,0):
            learning_rate_choices.append(10**i)
        
        #builds the parts of the model that are to be tuned over
        def build_model(hp):
            hp_learning_rate = hp.Choice('learning_rate', values = learning_rate_choices)
            model = tf.keras.Sequential() #using a sequential network for ANN
            model.add(tf.keras.layers.Dense(pulse_length))#input layer 
            #build the hidden layers
            for i in range(hp.Int('num layers',1,10)): #tunes the number of hidden layers
                model.add(tf.keras.layers.Dense(units=hp.Int('units_'+str(i),
                                                    min_value = 1,
                                                    max_value = 512,
                                                    step = 1),
                                                activation = hp.Choice('act_'+str(i),['relu','sigmoid','softmax','softplus','softsign','tanh','selu','elu'])))
            model.add(tf.keras.layers.Dense(1,activation='sigmoid')) #Always one node sigmoid
            
            model.compile( #builds each individuial model in tuning run
                optimizer=tf.keras.optimizers.Adam(learning_rate = hp_learning_rate),
                loss=tf.keras.losses.BinaryCrossentropy(from_logits=False),
                metrics=['accuracy'])
            return model

        callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=pat) #sets the early stopping parameters 
        #sets up the actuial tuning algorithum
        '''  
        tuner = kt.BayesianOptimization( 
            build_model,
            objective = 'val_accuracy',
            max_trials = trials,
            executions_per_trial = ex_per_trial,)
        tuner.search(self.trainPPdata,self.trainlabels,epochs=ep_per_trial, callbacks = [callback], validation_split = 0.2, verbose = 1) #does the hypertuning
        '''
        tuner = kt.Hyperband(
            build_model,
            objective='val_accuracy',
            max_epochs=ep_per_trial,
            factor=3,
            project_name = 'Pulse_ANN',
            hyperband_iterations=2) 
        #tuner.search(self.trainPPdata,self.trainlabels,epochs=ep_per_trial, callbacks = [callback], validation_split = 0.2, verbose = 1) #does the hypertuning
        self.model = tuner.get_best_models()[0] #saves the tuned model (pre trained) 

    def TrainModel(self, eps = 10, pat = 3):
        pp_ANN = self.model#loading in tuned, pre trined, model
        pp_ANN.compile(optimizer='nadam',
                  loss='binary_crossentropy',
                  metrics=['accuracy'])
        callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=pat) #sets the early stopping parameters 
        history = pp_ANN.fit(self.trainPPdata, self.trainlabels, epochs = eps ,validation_split = 0.2, verbose = 1, callbacks=[callback])
        self.model = pp_ANN
    
    def LoadModel(self, plot, modelpath):
        self.model = tf.keras.models.load_model(modelpath) #loads existing model
    
    def TestModel(self):
        #makes predictions on novel data
        predictions = self.model.predict(self.testPPdata)
        self.predictions = predictions

        #self.ROC_curve(predictions)
        
    def SaveModel(self, path):
        #saves model to disk for future use
        pp_ANN = self.model
        pp_ANN.save(path)
