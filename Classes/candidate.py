import numpy as np
from presto.prepfold import pfd
import matplotlib
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy import interpolate as interp
import mysql.connector as sq
from . import predictor
import configparser
import os

class CANDIDATE:
    def __init__(self,name='',DM=[],chi=[],RA=[],DEC=[],slist=[],plist=[],DMplot=[],SumProfFinal=[],p1=[],p2=[],p3=[],metadata=[]):
        #initilize all of the lists (with data from previous instances)
        self.DM = list(DM)
        self.chi = list(chi)
        self.RA = list(RA)
        self.DEC = list(DEC)
        self.slist = list(slist)
        self.plist = list(plist)
        self.DMplot = list(DMplot)
        self.SumProfFinal = list(SumProfFinal)
        self.p1 = list(p1)
        self.p2 = list(p2)
        self.p3 = list(p3)
        #setup the file in prepfold and metadata
        self.file = pfd(name)
        self.metadata = metadata
        self.metadata.append({})
        self.metadata[-1]["name"] = name
        self.name = name

    def configparser(self,directory,configfile):
        self.config = configparser.ConfigParser()
        os.chdir(directory)
        self.config.read(configfile)

    def db_add_pfd(self,src_directory):
        names = [dct["name"] for dct in self.metadata] #extracts the names from the list of dictionaries (metadate)
        for name in names
            self.cursor.execute("INSET INTO pfds VALUES('" + name.removesuffix(".pfd") + "', '" + src_directory + "', '" src_directory + "/" + name + "');") #adds the pfd to the database

    def db_add_png(self,img_directory):
        names = [dct["name"] for dct in self.metadata] #extracts the names from the list of dictionaries (metadate)
        for name in names
            self.cursor.execute("INSET INTO pfds VALUES('" + name.removesuffix(".pfd") + "', '" + img_directory + "', '" img_directory + "/" + name + ".png');") #adds the pfd to the database

    def db_connect(self,host,user,password,database):
        self.db = sq.connect( #connects us to mysql server
                host = host,
                user = user,
                password = password
                )
        self.cursor = self.db.cursor() #makes object to create queries
        self.cursor.execute("use " + database + ";") #connects us to the right database

    def make_plots(self, src_directory,img_directory):
        os.chdir(img_directory)
        os.system('show+pfd ' + src_directory + '/' + self.name) #makes a megaplot of the given pulsar note other files may be created

    def return_lists(self):
        return(self.DM, self.chi, self.RA, self.DEC, self.slist, self.plist, self.DMplot, self.SumProfFinal, self.p1, self.p2, self.p3, self.metadata)

    def greyscale(self,img):
        global_max = np.maximum.reduce(np.maximum.reduce(img))
        min_parts = np.minimum.reduce(img,1)
        img = (img-min_parts[:,np.newaxis])/global_max
        return img

    def clear(self):
        self.DM = []
        self.chi = []
        self.p1 = []
        self.p2 = []
        self.p3 = []
        self.RA = []
        self.DEC = []
        self.slist = []
        self.plist = []
        self.DMplot = []
        self.SumProfFinal = []
        self.metadata = []

    def unpack_and_interp(self):
        #pull out data from the file to our empty lists
        self.file = pfd(self.name)
        self.DM.append(self.file.bestdm)
        self.chi.append(self.file.calc_redchi2())
        self.p1.append(self.file.bary_p1)
        self.p2.append(self.file.bary_p2)
        self.p3.append(self.file.bary_p3)
        self.RA.append(self.file.rastr.decode('UTF-8'))
        self.DEC.append(self.file.decstr.decode('UTF-8'))

        #subband interpolation
        rawsubband = self.file.profs.sum(0)
        subband = self.greyscale(rawsubband)
        datalength = np.linspace(0,1,len(rawsubband.T))
        datadepth = self.file.subfreqs
        interpolation = interp.interp2d(datalength,datadepth,rawsubband,kind='linear') #makes all images the same size
        datalength = np.linspace(0,1,200)
        datadepth = np.linspace(200,400,128)
        subband = interpolation(datalength,datadepth)
        img = self.greyscale(subband)
        listimg = np.ndarray.tolist(img)
        self.slist.append(listimg)

        #persistence interpolation
        timephase = self.file.profs.sum(1)
        datalength = np.linspace(0,1,len(timephase.T))
        datadepth = np.linspace(0,1,len(timephase))
        interpolation = interp.interp2d(datalength,datadepth,timephase,kind='linear') #normalizes the persistence sizes
        #replaces the data length and depth with new normalized values 
        datalength = np.linspace(0,1,50) 
        datadepth = np.linspace(0,1,200)
        timephase = interpolation(datalength,datadepth)
        img = self.greyscale(timephase)
        listimg = np.ndarray.tolist(img)
        self.plist.append(listimg)

        #DM Plot
        DMdata = self.file.plot_chi2_vs_DM(np.min(self.file.dms), np.max(self.file.dms), 60, pfd) #creates the normalized DM plot
        DMlist = list(DMdata)
        self.DMplot.append(DMlist)

        #sum prof
        #TODO variable naming
        q = self.file.profs.sum(1).sum(0)/np.max(self.file.profs.sum(1).sum(0))
        Q = (q-np.min(q))/np.max(1-np.min(q))
        x = np.linspace(0,1,len(Q))
        f = interp.interp1d(x, Q)
        x = np.linspace(0,1,200)
        y = f(x)
        sumproflist = np.ndarray.tolist(y)
        self.SumProfFinal.append(sumproflist)

    def makedata(self,number=0):
        #this should only be done once per npy file
        #note, training labels are not included. TODO add an optional train labels function 
        self.pulsar_data = {
                "Best DM" : self.DM,
                "Chi" : self.chi,
                "p1" : self.p1,
                "p2" : self.p2,
                "p3" : self.p3,
                "RA" : self.RA,
                "DEC" : self.DEC,
                "Subband" : self.slist,
                "Persistence" : self.plist,
                "Sum Prof" : self.SumProfFinal,
                "DM Plots" : self.DMplot,
                "Metadata" : self.metadata
                "Metadata" : self.metadata,
                "Number" : number
        }
    def output(self,output_file):
        np.save(output_file,self.pulsar_data)
