from . import mla
from xgboost import XGBClassifier
from sklearn import metrics
from sklearn import utils
from sklearn import model_selection
from sklearn.model_selection import train_test_split
import numpy as np

class XGBoost(mla.MLA):
    def ModelFeatures(self):
        pass
    def BalanceData(self, datatype):
        if datatype == 'train':
            totaldatadict = self.traindatadict 
        elif datatype == 'test':
            totaldatadict = self.testdatadict
            
        self.DataBalanceCheck(datatype)
        poscount = self.pos
        negcount = self.neg
        print("poscount is ", poscount)
        print("negcount is ", negcount)
        print(negcount+poscount)
        baldatadict = {}
        labelsarray = []
        dmplotarray = []
        if negcount > poscount: 
            for i in range(int(poscount+poscount)): #indexing over entire length becuase I don't know how the data is shuffled
                labelsarray.append(totaldatadict['Train Labels'][i])
                dmplotarray.append(totaldatadict['DM Plots'][i])
            print("Labelsarray:",len(labelsarray))
            baldatadict['Train Labels'] = labelsarray
            print(len(baldatadict['Train Labels']))
            self.ncount = len(baldatadict['Train Labels'])
            print("DMplot array:",len(dmplotarray))
            baldatadict['DM Plots'] = dmplotarray  
            print(len(baldatadict['DM Plots']))
        elif negcount == poscount:
            baldatadict['Train Labels'] = totaldatadict['Train Labels']
            baldatadict['DM Plots'] = totaldatadict['DM Plots']
        if datatype == 'train':
            self.traindatadict = baldatadict
            self.trainbalance = True
        elif datatype == 'test':
            print("test")
            self.testdatadict = baldatadict
            self.testbalance = True
        
        #self.SaveBalancedData(datatype)
        
    def FlattenData(self, datatype):
        if datatype == 'train':
            dmdatadict = self.traindatadict 
        elif datatype == 'test':
            dmdatadict = self.testdatadict
        
        dmdata_array = np.array(dmdatadict['DM Plots']) #converting the DM data into an array (no need for labels)
        datalength = len(dmdata_array) #creates a variable with the number of candidates in the dataset
        dmdata_flat = dmdata_array.reshape(datalength,-1) # flattens the features of the data set into one axis 
        
        
        if datatype == 'train':
            self.traindmdata = dmdata_flat
            self.traindmlabels = dmdatadict['Train Labels']
            
        elif datatype == 'test':
            self.testdmdata = dmdata_flat
            self.testdmlabels = dmdatadict['Train Labels']
            
    def BuildModel(self):
        self.xgbmodel = XGBClassifier(n_estimators=2, max_depth=2, learning_rate=0.001, objective='binary:logistic')
        
    def TrainModel(self):
        model = self.xgbmodel
        model.fit(self.traindmdata,self.traindmlabels)
        self.xgbmodel = model
        
    def TestModel(self):
        model = self.xgbmodel
        probability = model.predict_proba(self.testdmdata)
        pulsarprobability = []
        self.testlabels = self.testdmlabels
        for i in range(len(probability)):
            pulsarprobability.append(probability[i][1])
        self.predictions = pulsarprobability
        self.ROC_Curve('ROC_Curves/DM_plots.png','DM','DM')
    def SaveModel(self, modelpath):
        self.xgbmodel.save_model(modelpath)
    def LoadModel(self, modelpath):
        self.xgbmodel.load_model(modelpath) #note that BuldModel must be run, but not TrainModel  
