from . import mla
import tensorflow as tf
import numpy as np

class PREDICTOR(mla.MLA):
    def LoadModel(self,modelpath):
        self.combinermodel = tf.keras.models.load_model(modelpath)

    def LoadData(self,datapath):
        self.datadict = np.load(datapath,allow_pickle=True)[()]

    def DataPrep(self):
        self.cnnsubdata = np.array(self.datadict['Subband'])[0]
        self.persdata2d = np.array(self.datadict['Persistence'])[0]
        self.dmdata2d = np.array(self.datadict['DM Plots'])[0]
        self.pulsedata = np.array(self.datadict['Sum Prof'])[0]
        print(self.dmdata2d)
        print("LEN", len(self.cnnsubdata)," ", len(self.dmdata2d))

    def Flatten(self):
        self.persdata1d = self.persdata2d.reshape(len(self.persdata2d),-1)
        self.dmdata1d = self.dmdata2d.reshape(len(self.dmdata2d),-1)
        self.sub1d = self.cnnsubdata.reshape(len(self.cnnsubdata),-1)

    def LoadInputModels(self,plots,modelpaths):
        #loads in all models
        self.models = []
        self.modelnames = []
        if 'subbandcnn' in plots:
            self.models.append(tf.keras.models.load_model(modelpaths[plots.index('subbandcnn')]))
            self.modelnames.append('subbandcnn')
        if 'pers1dcnn' in plots:
            self.models.append(tf.keras.models.load_model(modelpaths[plots.index('pers1dcnn')]))
            self.modelnames.append('pers1dcnn')
        if 'pers2dcnn' in plots:
            self.models.append(tf.keras.models.load_model(modelpaths[plots.index('pers2dcnn')]))
            self.modelnames.append('pers2dcnn')
        if 'dmcnn' in plots:
            self.models.append(tf.keras.models.load_model(modelpaths[plots.index('dmcnn')]))
            self.modelnames.append('dmcnn')
        if 'pulseann' in plots:
            self.models.append(tf.keras.models.load_model(modelpaths[plots.index('pulseann')]))
            self.modelnames.append('pulseann')
        if 'subbandcat' in plots:
            catmodel = CatBoostClassifier()
            catmodel.load_model(modelpaths[plots.index('subbandcat')])
            self.models.append(catmodel) 
            self.modelnames.append('subbandcat')

    def InputPredictions(self,model):
        #pull out the right model
        if (self.modelnames[self.models.index(model)] == 'pers1dcnn'):
            data = self.persdata1d
        elif (self.modelnames[self.models.index(model)] == 'pers2dcnn'):
            data = self.persdata2d
        elif (self.modelnames[self.models.index(model)] == 'subbandcnn'):
            data = self.cnnsubdata
        elif (self.modelnames[self.models.index(model)] == 'dmcnn'):
            data = self.dmdata1d
        elif (self.modelnames[self.models.index(model)] == 'subbandcat'):
            data = self.sub1d
        elif (self.modelnames[self.models.index(model)] == 'pulseann'):
            data = self.pulsedata
        elif (self.modelnames[self.models.index(model)] == 'pulsecat'):
            data = self.pulsedata
        elif (self.modelnames[self.models.index(model)] == 'dmxboost'):
            data = self.dmdata1d
        #predict on chosen model
        prediction = model.predict(data)
        return prediction

    def BuildInputArray(self):
        prediction_data = []
        for i in self.models:
            prediction_data.append(self.InputPredictions(i)) #make predictions from the component arrays
        #reshape the input arrays to be in the right order for predictions
        shape = np.shape(prediction_data)
        prediction_data = np.transpose(prediction_data)
        prediction_data = prediction_data.reshape((shape[1],shape[0]))
        self.inputpreds = prediction_data

    def Predictions(self):
        #function to make predictions on the unlabeld data
        self.BuildInputArray()
        print(self.inputpreds)
        self.predictions = self.combinermodel.predict(self.inputpreds)

    def SavePreds(self):
        #put the predictions into the metadata
        i = 0
        for n in self.datadict['Metadata']:
            n['Prediction'] = self.predictions[i]
            i+=1
    
    def SaveArray(self,outfile):
        np.save(outfile, self.datadict)
