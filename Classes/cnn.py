import tensorflow as tf
import keras_tuner as kt
from keras_tuner import BayesianOptimization
import numpy as np
from . import mla

class CNN(mla.MLA):
    def ModelFeatures(self):
        pass
    def BalanceData(self, datatype):
        #load in training data 
        if datatype == 'train':
            totaldatadict = self.traindatadict 
        #load in testing data 
        elif datatype == 'test':
            totaldatadict = self.testdatadict
        
        self.DataBalanceCheck(datatype) #make sure the data is balanced 
        #get the number of positives and negitives in the data
        poscount = self.pos
        negcount = self.neg
        #creates empty arrays for all of the plots
        baldatadict = {}
        labelsarray = []
        subplotarray = []
        persplotarray =[]
        dmplotarray = []
        if negcount > poscount: 
            for i in range(int(2*poscount)): #indexing over entire length becuase I don't know how the data is shuffled
                labelsarray.append(totaldatadict['Train Labels'][i])
                subplotarray.append(totaldatadict['Subband'][i])
                persplotarray.append(totaldatadict['Persistence'][i])
                dmplotarray.append(totaldatadict['DM Plots'][i])
            baldatadict['Train Labels'] = labelsarray
            self.ncount = len(baldatadict['Train Labels'])
            baldatadict['Subband'] = subplotarray  
            baldatadict['Persistence'] = persplotarray  
            baldatadict['DM Plots'] = dmplotarr11            
        elif negcount == poscount:
            baldatadict['Train Labels'] = totaldatadict['Train Labels']
            baldatadict['Subband'] = totaldatadict['Subband']
            baldatadict['Persistence'] =totaldatadict['Persistence']
            baldatadict['DM Plots'] =totaldatadict['DM Plots']
        
        if datatype == 'train':
            self.traindatadict = baldatadict
            self.trainbalance = True
        elif datatype == 'test':
            self.testdatadict = baldatadict
            self.testbalance = True
            
    def SaveData(self,datatype):
        if datatype == 'train':
            np.save("CNNTrainSet.npy", self.traindatadict)
        elif datatype == 'test':
            np.save("CNNTestSet.npy", self.testdatadict)
        #just save as dict, in init add user specified file name
    
    def FlattenData(self, datatype): #for the two 1D CNNs
        if datatype == 'train':
            datadict = self.traindatadict 
        elif datatype == 'test':
            datadict = self.testdatadict
        
        persdata_array = np.array(datadict['Persistence']) #converting the DM data into an array (no need for labels)
        persdatalength = len(persdata_array) #creates a variable with the number of candidates in the dataset
        persdata_flat = persdata_array.reshape(persdatalength,-1) # flattens the features of the data set into one axis 
        
        dmdata_array = np.array(datadict['DM Plots'])
        dmdatalength = len(dmdata_array)
        dmdata_flat = dmdata_array.reshape(dmdatalength,-1)
        
        #creates the variables for the flat data 
        if datatype == 'train':
            self.train1dpersdata = persdata_flat
            self.traindmdata = dmdata_flat
            
        elif datatype == 'test':
            self.test1dpersdata = persdata_flat
            self.testdmdata = dmdata_flat
            
    def DataPrep(self):
            #train data
            datadict = self.traindatadict
            self.trainlabels = np.array(datadict['Train Labels'])
            self.trainsubdata = np.array(datadict['Subband'])
            self.train2dpersdata = np.array(datadict['Persistence'])
            self.traindmdata = np.array(datadict['DM Plots'])
            #test data
            datadict = self.testdatadict
            self.testlabels = np.array(datadict['Train Labels'])
            self.testsubdata = np.array(datadict['Subband'])
            self.test2dpersdata = np.array(datadict['Persistence'])   
            self.testdmdata = np.array(datadict['DM Plots'])        
                
    def Build1DModel(self,plot):
        model = tf.keras.Sequential(Input((120,1))[tf.keras.layers.Conv1D(64, 6, activation='relu'), 
            tf.keras.layers.MaxPooling1D(2), 
            tf.keras.layers.Conv1D(32, 3, activation='relu'), 
            tf.keras.layers.MaxPooling1D(1),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(10, activation='relu'),
            tf.keras.layers.Dense(1, activation='sigmoid')]) #output layer, 1 node 
        self.plot = plot
        
        if plot == 'Persistence_1D':
            self.pers1dmodel = model
        if plot == 'DM Plots':
            self.dmmodel = model
           
    
    def Hypertune1D(self,train,in_shape,trainlabels,plot,trials,ep_per_trial,ex_per_trial,pat):
        activations = ['relu','sigmoid','softmax','softplus','softsign','tanh','selu','elu']
        learning_rate_choices = []
        for i in range(-5,0):
            learning_rate_choices.append(10**i)
        def build_model(hp):
            hp_learning_rate = hp.Choice('learning_rate', values = learning_rate_choices)
            size = hp.Int('input filter size',3,20)
            model = tf.keras.Sequential()
            model.add(tf.keras.layers.Conv1D(hp.Int('input_filters',3,64), size, activation= hp.Choice('input activation', activations),input_shape =(in_shape)))
            sizes = []
            sizes.append(size)
            for i in range(hp.Int('num_hidden_layers',1,15)):
                pooling = hp.Int('pooling size ' + str(i),1,(size-2))
                model.add(tf.keras.layers.MaxPooling1D(pooling))
                size = hp.Int('filter size ' + str(i),3,min(sizes))
                sizes.append(size)
                model.add(tf.keras.layers.Conv1D(hp.Int('filters' + str(i) ,3,64), size, activation= hp.Choice('activation ' + str(i),activations)))
            pooling = hp.Int('pooling size final',1,(size-2))
            model.add(tf.keras.layers.MaxPooling1D(pooling))
            model.add(tf.keras.layers.Flatten())
            model.add(tf.keras.layers.Dense(1,activation='sigmoid'))
            model.compile(
                optimizer=tf.keras.optimizers.Adam(learning_rate = hp_learning_rate),
                loss=tf.keras.losses.BinaryCrossentropy(from_logits=False),
                metrics=['accuracy'])
            return model
        callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=int(pat)) #sets the early stopping parameters 
        tuner = kt.BayesianOptimization(
            build_model,
            objective = 'val_accuracy',
            max_trials = int(trials), #the number of model versions we make
            project_name = plot + '_CNN',
            executions_per_trial=int(ex_per_trial),) #can add project_name
        callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=int(pat)) #sets the early stopping parameters 
        tuner.search(train,trainlabels, epochs=int(ep_per_trial), validation_split=0.2, callbacks = [callback]) #runs optimizations 
        return tuner
 
    def Hypertune2D(self,train,in_shape,trainlabels,plot,trials,ep_per_trial,ex_per_trial,pat):
        activations = ['relu','sigmoid','softmax','softplus','softsign','tanh','selu','elu']
        learning_rate_choices = []
        for i in range(-5,0):
            learning_rate_choices.append(10**i)
        def build_model(hp):
            hp_learning_rate = hp.Choice('learning_rate', values = learning_rate_choices)
            size = hp.Int('input filter size',3,20)
            model = tf.keras.Sequential()
            model.add(tf.keras.layers.Conv2D(hp.Int('input_filters',3,64), (size,size), activation= hp.Choice('input activation',activations), input_shape=(in_shape)))
            sizes = [] #keeping track of the sizes we have used so we only go down in size
            sizes.append(size)
            for i in range(hp.Int('num_hidden_layers',1,15)):
                pooling = hp.Int('pooling size ' + str(i),1,(size-2))
                model.add(tf.keras.layers.MaxPooling2D((pooling,pooling)))
                size = hp.Int('filter size ' + str(i),3,min(sizes))
                sizes.append(size)
                model.add(tf.keras.layers.Conv2D(hp.Int('filters' + str(i) ,3,64), (size,size), activation=hp.Choice('activation ' + str(i),activations)))
            pooling = hp.Int('pooling size final',1,(size-2))
            model.add(tf.keras.layers.MaxPooling2D((pooling,pooling)))
            model.add(tf.keras.layers.Flatten())
            model.add(tf.keras.layers.Dense(1,activation='sigmoid'))
            model.compile(
                optimizer=tf.keras.optimizers.Adam(learning_rate = hp_learning_rate),
                loss=tf.keras.losses.BinaryCrossentropy(from_logits=False),
                metrics=['accuracy'])
            return model
        callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=int(pat)) #sets the early stopping parameters 
        tuner = kt.BayesianOptimization(
            build_model,
            objective = 'val_accuracy',
            max_trials = int(trials), #the number of model versions we make
            project_name = plot + '_1DCNN',
            executions_per_trial=int(ex_per_trial),) #can add project_name
        callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=int(pat)) #sets the early stopping parameters 
        tuner.search(train,trainlabels, #runs the optimization
                    epochs=int(ep_per_trial),
                    validation_split=0.2,
                    callbacks = [callback]) #epochs per trial train. Should probably make bigger, or a variable

        return tuner
        
  
    def Hypertune(self,plot,trials=150,ep_per_trial=10,ex_per_trial=5,pat = 3):
        #gets the shape of a single plot
        print('PLOT: ', plot)
        if(plot == 'Subband'):
            train = self.trainsubdata
            in_shape = (128,200,1)
            trainlabels = self.trainlabels
            tuner = self.Hypertune2D(train,in_shape,trainlabels,plot,trials,ep_per_trial,ex_per_trial,pat)
        elif(plot == 'Persistence_2D'):
            train = self.train2dpersdata
            in_shape = (200,50,1)
            trainlabels = self.trainlabels
            tuner = self.Hypertune2D(train,in_shape,trainlabels,plot,trials,ep_per_trial,ex_per_trial,pat)
        elif(plot == 'Persistence_1D'):
            train = self.train1dpersdata
            in_shape = (10000,1)
            trainlabels = self.trainlabels
            tuner = self.Hypertune1D(train,in_shape,trainlabels,plot,trials,ep_per_trial,ex_per_trial,pat)
        elif(plot == 'DM Plots'):
            train = self.traindmdata
            in_shape = (120,1)
            trainlabels = self.trainlabels
            tuner = self.Hypertune1D(train,in_shape,trainlabels,plot,trials,ep_per_trial,ex_per_trial,pat)
       
        #saves the model in the apropreate place
        
        if plot == 'Subband':
            self.plot = 'Subband'
            self.submodel = tuner.get_best_models()[0]
        elif plot == 'Persistence_2D':
            self.plot = 'Persistence_2D'
            self.pers2dmodel = tuner.get_best_models()[0]
        elif plot == 'Persistence_1D':
            self.plot = 'Persistence_1D'
            self.pers1dmodel = tuner.get_best_models()[0]
        elif plot == 'DM Plots':
            self.plot = 'DM Plots'
            self.dmmodel = tuner.get_best_models()[0]
        
        
    def Build2DModel(self,plot):
        if plot == 'Subband':
            inputshape = (128,200,1)
        if plot == 'Persistence_2D':
            inputshape=(200, 50, 1)
           

        model = tf.keras.Sequential()
        model.add(tf.keras.layers.Conv2D(32,(3,3),activation='relu',input_shape=(inputshape)))
        model.add(tf.keras.layers.MaxPooling2D((2,2)))
        model.add(tf.keras.layers.Flatten())
        model.add(tf.keras.layers.Dense(1,activation='sigmoid'))
        #model2d = tf.keras.Sequential([tf.keras.layers.Conv2D(input_shape=inputshape,32, (3, 3), activation='relu', #layer with 112 nodes
            #tf.keras.layers.MaxPooling2D((2, 2)), #layer with 112 nodes
            #tf.keras.layers.Flatten(),
            #tf.keras.layers.Dense(10, activation='relu'),
            #tf.keras.layers.Dense(1, activation='sigmoid'))]) #output layer, 1 node 
        
        if plot == 'Subband':
            self.plot = 'Subband'
            self.submodel = model
        if plot == 'Persistence_2D':
            self.plot = 'Persistence_2D'
            self.pers2dmodel = model
    
    def TrainModel(self, eps = 10, pat = 3, persdim = None):
        trainlabels = self.trainlabels 
        if self.plot == 'Subband':
            model = self.submodel
            train = self.trainsubdata
        elif self.plot == 'Persistence_2D':
            model = self.pers2dmodel
            train = self.train2dpersdata
        elif self.plot == 'Persistence_1D':
            model = self.pers1dmodel
            train = self.train1dpersdata
        elif self.plot == 'DM Plots':
            model = self.dmmodel
            train = self.traindmdata
        callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=int(pat)) #sets the early stopping parameters 
        model.compile(optimizer='adam',
              loss=tf.keras.losses.BinaryCrossentropy(from_logits=False),
              metrics=['accuracy'])
        history = model.fit(train, trainlabels, epochs = int(eps), 
                    validation_split = 0.2, batch_size = 1,
                    callbacks = [callback])
        if self.plot == 'Subband':
            self.historysubmodel = history
        if self.plot == 'Persistence_2D':
            self.history2dmodel = history
        if self.plot == 'Persistence_1D':
            self.history1dmodel = history
        if self.plot == 'DM Plots':
            self.historydmmodel = history
            
    def LoadModel(self, plot, modelpath):
        model = tf.keras.models.load_model(modelpath)
        self.plot = plot
        if plot == 'Subband':
            self.submodel = model
        if plot == 'Persistence_2D':
            self.pers2dmodel = model
        if plot == 'Persistence_1D':
            self.pers1dmodel = model
        if plot == 'DM Plots':
            self.dmmodel = model
    def TestModel(self):
        if self.plot == 'Subband':
            model = self.submodel
            test = self.testsubdata
        if self.plot == 'Persistence_2D':
            model = self.pers2dmodel
            test = self.test2dpersdata
        if self.plot == 'Persistence_1D':
            model = self.pers1dmodel
            test = self.test1dpersdata 
        if self.plot == 'DM Plots':
            model = self.dmmodel
            test = self.testdmdata
        model.summary()
        predictions = model.predict(test)
        self.predictions = predictions 
    
    def SaveModel(self, modelpath, persdim = None):
        if self.plot == 'Subband':
            model = self.submodel
        if self.plot == 'Persistence_2D':
            model = self.pers2dmodel
        if self.plot == 'Persistence_1D':
            model = self.pers1dmodel
        if self.plot == 'DM Plots':
            model = self.dmmodel
        model.save(modelpath) 
