import numpy as np
import tensorflow as tf
from tensorflow import keras
#from xgboost import XGBClassifier
import json
from sklearn import metrics
from sklearn import utils
from sklearn import model_selection
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
#from catboost import CatBoostClassifier, Pool, cv
#from catboost import metrics as catmet

class MLA(object):
    def __init__(self, trainnonpdatafile = None, testnonpdatafile = None, random_state = None):
        self.randomstate = random_state
        self.trainshuffle = False
        self.trainbalance = False
        self.testshuffle = False
        self.testbalance = False
    
    def SetDevice(self,index=0,device='GPU',mem = 29000):
        #ask tensorflow to only use one GPU (because it can't take full advantage of both at once)
        devices = tf.config.list_physical_devices(device)
        if devices:
            tf.config.set_visible_devices(devices[index], device)
        for gpu in devices:
           tf.config.experimental.set_memory_growth(gpu,True)

    def LoadArray(self, array, datatype='test'): #distinct from LoadData insofar as it takes a numpy array, not a file
        if isinstance(array, dict):
            if datatype =='test':
                self.testdatadict = array
            elif datatype == 'train':
                self.traindatadict = array
        else:
            if datatype == 'test':
                self.testdatadict = array[()]
            elif datatype == 'train':
                self.traindatadict = array[()]
    
    def LoadData(self, datatype, datapath):
        #Method that results in a single pulsar and non pulsar dictionary. 
        #First, check if loading in the training or testing dataset. Then sets it to the 
        #general data variable used in method.
        if datatype == 'train':
            self.traindatafile = datapath
            datafile = self.traindatafile
        elif datatype == 'test': 
            self.testdatafile = datapath
            datafile = self.testdatafile
            
        totaldatadict = np.load(datafile,allow_pickle=True)
        if isinstance(totaldatadict, dict): #directly sets the train/test attribute if data already formatted
            if datatype == 'train':
                self.traindatadict = totaldatadict
            elif datatype == 'test':
                self.testdatadict = totaldatadict
        else: #else converts to dictionary then sets the attributes.
            if datatype == 'train':
                self.traindatadict = totaldatadict[()]
            elif datatype == 'test':
                self.testdatadict = totaldatadict[()] 
        
    def ShuffleData(self,datatype, randomstate=42):
        if datatype == 'train':
            datadict = self.traindatadict 
        elif datatype == 'test':
            datadict = self.testdatadict
        for key in datadict.keys():
            np.random.seed(randomstate)
            np.random.shuffle(datadict[key])
        if datatype == 'train':
            self.traindatadict = datadict
            self.trainshuffle = True
        elif datatype == 'test':
            self.testdatadict = datadict
            self.testshuffle = True
        
    def DataBalanceCheck(self, datatype):
        if datatype == 'train':
            totaldatadict = self.traindatadict 
        elif datatype == 'test':
            totaldatadict = self.testdatadict
        
        for key in totaldatadict.keys():
            print(key)
        posvalues = 0
        negvalues = 0
        for i in range(len(totaldatadict['Train Labels'])):
            if totaldatadict['Train Labels'][i] == 1.0:
                posvalues = posvalues + 1.0
            elif totaldatadict['Train Labels'][i] == 0.0:
                negvalues = negvalues +1.0
        pospercent = (posvalues/len(totaldatadict['Train Labels']))*100
        negpercent = (negvalues/len(totaldatadict['Train Labels']))*100
        self.pos = posvalues
        self.neg = negvalues
        self.pospercent = pospercent
        self.negpercent = negpercent
        self.datamix = "Data set is {}% pulsar and {}% nonpulsar.".format(pospercent, negpercent)
        
    
    def BalanceData(self, datatype):
        if datatype == 'train':
            totaldatadict = self.traindatadict 
        elif datatype == 'test':
            totaldatadict = self.testdatadict
        self.DataBalanceCheck(datatype)
        poscount = self.pos
        negcount = self.neg
        baldatadict = {}
        labelsarray = []
        profarray = []
        keylist = []
        if negcount > poscount: 
            negkeep = 0 #initializing the number of entries that have been deleted
            for i in range(int(poscount+poscount)): #indexing over entire length becuase I don't know how the data is shuffled
                labelsarray.append(totaldatadict['Train Labels'][i])
                profarray.append(totaldatadict['Sum Prof'][i])
            baldatadict['Train Labels'] = labelsarray
            self.ncount = len(baldatadict['Train Labels'])
            baldatadict['Sum Prof'] = profarray  
        elif negcount == poscount:
            baldatadict = totaldatadict
        if datatype == 'train':
            self.traindatadict = baldatadict
            self.trainbalance = True
        elif datatype == 'test':
            print("test")
            self.testdatadict = baldatadict
            self.testbalance = True
        
    def Normalize(self,datatype,plot):
        #get out the data we are normalizing
        if(plot == 'DM Plots'):
            if(datatype == 'train'):
                data = self.traindmdata
            elif(datatype == 'test'):
                data = self.traindmdata
        
        for case in range(len(data)): #goes through every instance in the dataset
            maximum = np.max(data[case][0]) #gets the maximum DM case ([0] is reduced chi squared)
            data[case] = (data[case])/maximum
            
    def SaveData(self,datatype):
        if datatype == 'train':
            np.save("TrainSet.npy", self.traindatadict)
        elif datatype == 'test':
            np.save("TestSet.npy", self.testdatadict)
        #just save as dict, in init add user specified file name
    
    def MultiROC(self,plot,name,colour):
        #this function draws an roc curve without clearing so they will be drawn on top of one another saving is done in another function
        #takes in predictions bc there will be too many to be an attribute
        falsepos, truepos, thresholds = metrics.roc_curve(self.testlabels, self.predictions)
        auc = metrics.auc(falsepos, truepos)
        roc = metrics.roc_curve(self.testlabels, self.predictions) 
        print("AUC Score:", auc)
        X = np.arange(0,1,0.01)
        plt.xlim([0,1])
        plt.ylim([0,1])
        plt.plot(X,X,color = 'black',linestyle = 'dashed') #adds random classifier 
        plt.ylabel('True Positives')
        plt.xlabel('False Positives')
        key = name + "(AUC: " + str(round(auc,3)) + ")"
        plt.plot(roc[0],roc[1],color=colour, label = key) 

    def MultiROCSave(self,filename):
        plt.ylabel('True Positives')
        plt.xlabel('False Positives')
        plt.title('Combined ROC curve')
        plt.legend()
        plt.savefig(filename)
        plt.clf() #clears fig in case we are making more than one 

    def ROC_Curve(self, filename, plot, name):
        #takes in predictions bc there will be too many to be an attribute
        falsepos, truepos, thresholds = metrics.roc_curve(self.testlabels, self.predictions)
        auc = metrics.auc(falsepos, truepos)
        roc = metrics.roc_curve(self.testlabels, self.predictions) 
        print("AUC Score:", auc)
        X = np.arange(0,1,0.01)
        plt.xlim([0,1])
        plt.ylim([0,1])
        plt.plot(X,X,color = 'black',linestyle = 'dashed') #adds random classifier 
        plt.ylabel('True Positives')
        plt.xlabel('False Positives')
        plt.plot(roc[0],roc[1]) 
        plt.suptitle('ROC Curve for ' + name ,fontsize = 15)
        plt.title('AUC = ' + str(round(auc,3)), fontsize = 10)
        plt.savefig(filename)
        print(plot)
        print(name)
        plt.clf() #clears figure. Needed for making multible ROC plots at once
