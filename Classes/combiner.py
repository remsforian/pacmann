from . import mla
import numpy as np
import tensorflow as tf
from tensorflow import keras
from xgboost import XGBClassifier
import json
from sklearn import metrics
from sklearn import utils
from sklearn import model_selection
import matplotlib.pyplot as plt 
from sklearn.model_selection import train_test_split
from catboost import CatBoostClassifier, Pool, cv
from catboost import metrics as catmet
from keras import backend as K
import keras_tuner as kt

class COMBINER(mla.MLA):
    def ModelFeatures(self):
        pass
    def BalanceData(self, datatype):
        #load in training data 
        if datatype == 'train':
            totaldatadict = self.traindatadict 
        #load in testing data 
        elif datatype == 'test':
            totaldatadict = self.testdatadict
        
        self.DataBalanceCheck(datatype) #make sure the data is balanced 
        #get the number of positives and negitives in the data
        poscount = self.pos
        negcount = self.neg
        #creates empty arrays for all of the plots
        baldatadict = {}
        labelsarray = []
        subplotarray = []
        persplotarray =[]
        dmplotarray = []
        pulsearray = []
        if negcount > poscount: 
            for i in range(int(poscount+poscount)): #indexing over entire length becuase I don't know how the data is shuffled
                labelsarray.append(totaldatadict['Train Labels'][i])
                subplotarray.append(totaldatadict['Subband'][i])
                persplotarray.append(totaldatadict['Persistence'][i])
                dmplotarray.append(totaldatadict['DM Plots'][i])
                pulsearray.append(totaldatadict['Sum Prof'][i]) 
            baldatadict['Train Labels'] = labelsarray
            self.ncount = len(baldatadict['Train Labels'])
            baldatadict['Subband'] = subplotarray  
            baldatadict['Persistence'] = persplotarray  
            baldatadict['DM Plots'] = dmplotarray
            baldatadict['Sum Prof'] = pulsearray
        elif negcount == poscount:
            baldatadict['Train Labels'] = totaldatadict['Train Labels']
            baldatadict['Subband'] = totaldatadict['Subband']
            baldatadict['Persistence'] =totaldatadict['Persistence']
            baldatadict['DM Plots'] =totaldatadict['DM Plots']
            baldatadict['Sum Prof'] = totaldatadict['Sum Prof']       
 
        if datatype == 'train':
            self.traindatadict = baldatadict
            self.trainbalance = True
        elif datatype == 'test':
            self.testdatadict = baldatadict
            self.testbalance = True
   
    def LoadInputModels(self,plots,modelpaths):
        #loads in all models
        self.models = []
        self.modelnames = []
        if 'subbandcnn' in plots:
            self.models.append(tf.keras.models.load_model(modelpaths[plots.index('subbandcnn')]))
            self.modelnames.append('subbandcnn')
        if 'pers1dcnn' in plots:
            self.models.append(tf.keras.models.load_model(modelpaths[plots.index('pers1dcnn')]))
            self.modelnames.append('pers1dcnn')
        if 'pers2dcnn' in plots:
            self.models.append(tf.keras.models.load_model(modelpaths[plots.index('pers2dcnn')]))
            self.modelnames.append('pers2dcnn')
        if 'dmcnn' in plots:
            self.models.append(tf.keras.models.load_model(modelpaths[plots.index('dmcnn')]))
            self.modelnames.append('dmcnn')
        if 'pulseann' in plots:
            self.models.append(tf.keras.models.load_model(modelpaths[plots.index('pulseann')]))
            self.modelnames.append('pulseann')
        if 'subbandcat' in plots:
            catmodel = CatBoostClassifier()
            catmodel.load_model(modelpaths[plots.index('subbandcat')])
            self.models.append(catmodel) 
            self.modelnames.append('subbandcat')
        if 'pulsecat' in plots:
            pulsecatmodel = CatBoostClassifier()
            pulsecatmodel.load_model(modelpaths[plots.index('pulsecat')])
            self.models.append(pulsecatmodel)
            self.modelnames.append('pulsecat')
        if 'dmxboost' in plots:
            dmxgmodel = XGClassifier()
            dmxgmodel.load_model(modelpasths[plots.index('dmxboost')])
            self.models.append(dmxgmodel)
            self.modelnames.append('dmxboost')
    def dataprep(self):
        #split training data into variables
        if hasattr(self,'traindatadict'):
            datadict = self.traindatadict
            self.trainlabels = np.array(datadict['Train Labels'])
            self.traincnnsubdata = np.array(datadict['Subband'])
            self.train2dpersdata = np.array(datadict['Persistence'])
            self.train2ddmdata = np.array(datadict['DM Plots'])
            self.trainpulsedata = np.array(datadict['Sum Prof'])      

        #split testing data into variables 
        elif hasattr(self,'testdatadict'):
            datadict = self.testdatadict
            try: #allows for unlabled datasets
                self.testlabels = np.array(datadict['Train Labels'])
            except:
                pass
            self.testcnnsubdata = np.array(datadict['Subband'])
            self.test2dpersdata = np.array(datadict['Persistence'])   
            self.test2ddmdata = np.array(datadict['DM Plots']) 
            self.testpulsedata = np.array(datadict['Sum Prof'])

    def flatten(self):
        #flatten the datatypes that get flattened
        try:
            self.train1dpersdata = self.train2dpersdata.reshape(len(self.train2dpersdata),-1)   
            self.test1dpersdata = self.test2dpersdata.reshape(len(self.test2dpersdata),-1)   
            self.train1ddmdata = self.train2ddmdata.reshape(len(self.train2ddmdata),-1)
        except:
            pass
        try:
            self.test1ddmdata = self.test2ddmdata.reshape(len(self.test2ddmdata),-1)
            self.train1dsub = self.traincnnsubdata.reshape(len(self.traincnnsubdata),-1)
            self.test1dsub = self.testcnnsubdata.reshape(len(self.testcnnsubdata),-1)    
        except:
            pass
     
    def prediction(self,model,datatype):
        #pull out the right model
        if self.modelnames[self.models.index(model)] == 'pers1dcnn':
            if datatype=='train':
                train = self.train1dpersdata
            elif datatype=='test':
                test = self.test1dpersdata
        elif self.modelnames[self.models.index(model)] == 'pers2dcnn':
            if datatype=='train':
                train = self.train2dpersdata
            elif datatype=='test':
                test = self.test2dpersdata
        elif self.modelnames[self.models.index(model)] == 'subbandcnn':
            if datatype=='train':
                train = self.traincnnsubdata
            elif datatype=='test':
                test = self.testcnnsubdata
        elif self.modelnames[self.models.index(model)] == 'dmcnn':
            if datatype=='train':
                train = self.train1ddmdata
            elif datatype=='test':
                test = self.test1ddmdata
        elif self.modelnames[self.models.index(model)] == 'subbandcat':
            if datatype=='train':
                train = self.train1dsub
            elif datatype=='test':
                test = self.test1dsub
        elif self.modelnames[self.models.index(model)] == 'pulseann':
            if datatype=='train':
                train = self.trainpulsedata
            elif datatype=='test':
                test = self.testpulsedata
        elif self.modelnames[self.models.index(model)] == 'pulsecat':
            if datatype=='train':
                train = self.trainpulsedata
            elif datatype=='test':
                test = self.testpulsedata
        elif self.modelnames[self.models.index(model)] == 'dmxboost':
            if datatype=='train':
                train = self.train1ddmdata
            elif datatype=='test':
                test = self.test1ddmdata

        outmodel = keras.models.clone_model(model) #allows for multible runs to happen with same instance
        outmodel.pop() #removes the output layer of the network
        if (datatype == 'test'):
            prediction = outmodel.predict(test)
        elif (datatype == 'train'):
            prediction = outmodel.predict(train)
        return prediction

    def BuildInputArray(self,datatype,trials=150,ep_per_trial=10,ex_per_trial=3,pat=3):
        if datatype == 'train':
            prediction_data = np.empty((len(self.traincnnsubdata),0)) #makes an empty array with the same length as the input data
        if datatype == 'test':
            prediction_data = np.empty((len(self.testcnnsubdata),0)) #makes an empty array with the same length as the input data
        
        #pakes predictions with each MLA and makes an array of the second to last layer. Done to increase cobiner efficacy
        #note this behaviour only works with the tensorflow models 
        for i in self.models:
            prediction_data = np.column_stack((prediction_data,self.prediction(i,datatype)))
        if(datatype == 'train'): 
            self.trainpreds = prediction_data 
        if(datatype == 'test'):
            self.testpreds = prediction_data

   
    def ClassicalCombinerBuild(self,epochs=10,executions=5,pat=3):
       #This is the version not hypertuned, partly for debugging and partly for speed
        train = self.trainpreds
        learning_rate = 10**-3 
        #Single layer because combiner doesn't need to be complex
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.Flatten(input_shape=(len(self.models),))) #input layer
        model.add(tf.keras.layers.Dense(units=50, activation = 'relu')) #hidden layer
        model.add(tf.keras.layers.Dense(units=1,activation = 'sigmoid')) #output layer
        model.compile(
            optimizer=tf.keras.optimizers.Adam(learning_rate = learning_rate),
            loss=tf.keras.losses.BinaryCrossentropy(from_logits=False),
            metrics=['accuracy'])
        self.combinermodel = model
         
    def BuildCombiner(self,trials=150,ep_per_trial=10,ex_per_trial=5,pat=3):
        #build the train input function
        train = self.trainpreds
        #make list of learning rates to choose from 
        learning_rate_choices = []
        for i in range (-5,0): 
            learning_rate_choices.append(10**i)
        
        def build_model(hp):
            hp_learning_rate = hp.Choice('learning_rate', values = learning_rate_choices)
            model = tf.keras.Sequential()
            model.add(tf.keras.layers.Flatten(input_shape=(len(self.trainpreds[0]),)))#input layer, change number to size of input 
            for i in range(hp.Int('num layers',1,10)):
                model.add(tf.keras.layers.Dense(units=hp.Int('units_'+str(i),
                                                    min_value=1,
                                                    max_value = 512,
                                                    step=1),
                                            activation = hp.Choice('act_'+str(i),['relu','sigmoid','softmax','softplus','softsign','tanh','selu','elu'])))
            model.add(tf.keras.layers.Dense(1,activation='sigmoid')) #output layer

            model.compile(
                optimizer=tf.keras.optimizers.Adam(learning_rate = hp_learning_rate),
                loss=tf.keras.losses.BinaryCrossentropy(from_logits=False),
                metrics=['accuracy'])
            return model
        #tunes the model using Bayesion Optimization
        tuner = kt.BayesianOptimization(
            build_model,
            objective = 'val_accuracy',
            max_trials = trials,
            executions_per_trial = int(ex_per_trial),)
        '''
        tuner = kt.Hyperband(
                build_model,
                objective = 'val_accuracy',
                max_epochs=int(ep_per_trial),
                factor=2)
        '''
        callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=int(pat)) #sets the early stopping parameters
        tuner.search(train,self.trainlabels,
            epochs=ep_per_trial,
            validation_split=0.2,
            callbacks=[callback])
        #saves the model
        self.combinermodel = tuner.get_best_models()[0]

    def TrainModel(self,eps = 50,pat = 3, vsplit = 0.2, bsize = 1):
        trainlabels = self.trainlabels
        data = self.trainpreds
        model = self.combinermodel
        callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss',patience=int(pat)) #early stopping paramiters
         
        #compile the model for training
        model.compile(optimizer='adam',
            loss = tf.keras.losses.BinaryCrossentropy(from_logits=False),
            metrics = ['accuracy'])
        #train model and save trained model to history
        history = model.fit(data,trainlabels,epochs = int(eps), validation_split = float(vsplit), batch_size = int(bsize),callbacks = [callback])
        self.combinermodel = model 

    def TestModel(self):
        self.predictions = self.combinermodel.predict(self.testpreds) #makes a prediction with the preped data 
  
    def SaveModel(self,modelpath):
        model = self.combinermodel
        model.save(modelpath)

    def LoadModel(self,modelpath):
        self.combinermodel = tf.keras.models.load_model(modelpath)
    
    def ROC_Curve(self, filename, plot, name):
        #takes in predictions bc there will be too many to be an attribute
        falsepos, truepos, thresholds = metrics.roc_curve(self.testlabels, self.predictions)
        auc = metrics.auc(falsepos, truepos)
        roc = metrics.roc_curve(self.testlabels, self.predictions) 
        print("AUC Score:", auc)
        X = np.arange(0,1,0.01)
        plt.xlim([0,1])
        plt.ylim([0,1])
        plt.plot(X,X,color = 'black',linestyle = 'dashed') #adds random classifier 
        plt.ylabel('True Positives')
        plt.xlabel('False Positives')
        PLT.PLOT(ROC[0],ROC[1]) 
        plt.suptitle('ROC Curve for ' + name ,fontsize = 15)
        plt.title('AUC = ' + str(round(auc,3)), fontsize = 10)
        plt.savefig(filename)
        print(plot)
        print(name)
        plt.clf() #clears figure. Needed for making multible ROC plots at once

    def SavePreds(self):
        #add a prediction col to datadict
        self.traindatadict['predictions']=self.predictions
